onerror {exit -code 1}
vlib work
vlog -work work PecherskiIV211.vo
vlog -work work Waveform1.vwf.vt
vsim -novopt -c -t 1ps -L fiftyfivenm_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate_ver -L altera_lnsim_ver work.PecherskiIV211_2_vlg_vec_tst -voptargs="+acc"
vcd file -direction PecherskiIV211.msim.vcd
vcd add -internal PecherskiIV211_2_vlg_vec_tst/*
vcd add -internal PecherskiIV211_2_vlg_vec_tst/i1/*
run -all
quit -f
