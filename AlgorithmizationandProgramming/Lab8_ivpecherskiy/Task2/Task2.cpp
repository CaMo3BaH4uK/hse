/*
    * Microsoft Visual Studio 2022
    * MSVC C++ Complier (v143)
    * C++ Language Standart: ISO C++20 Standard
    *
    * Lab 8_2
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#define NOMINMAX

#include <iostream>
#include <Windows.h>
#include <limits>

#define MAX_ARRAY_LEN 255

void InputMatrix(int Matrix[MAX_ARRAY_LEN][MAX_ARRAY_LEN], int *N, int *M)
{
    std::cout << "������� ���������� ��������� �� ��������� (> 0 � < " << MAX_ARRAY_LEN << "):\n";
    std::cin >> *N;
    while (std::cin.fail() || *N < 1 || *N > MAX_ARRAY_LEN) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "��� ���� �� �������� ����������� ���������\n";
        std::cout << "������� ���������� ��������� �� ��������� (> 0 � < " << MAX_ARRAY_LEN << "):\n";
        std::cin >> *N;
    }

    std::cout << "������� ���������� ��������� �� ����������� (> 0 � < " << MAX_ARRAY_LEN << "):\n";
    std::cin >> *M;
    while (std::cin.fail() || *M < 1 || *M > MAX_ARRAY_LEN) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "��� ���� �� �������� ����������� ���������\n";
        std::cout << "������� ���������� ��������� �� ����������� (> 0 � < " << MAX_ARRAY_LEN << "):\n";
        std::cin >> *M;
    }

    for (int i = 0; i < *N; Matrix++ && i++) {
        int* Line = *Matrix;
        for (int j = 0; j < *M; Line++ && j++) {
            std::cout << "������� ������� " << i + 1 << " " << j + 1 << ":\n";
            std::cin >> *Line;
            while (std::cin.fail()) {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << "��� ���� �� �������� ���������\n";
                std::cout << "������� ������� " << i + 1 << ":\n";
                std::cin >> *Line;
            }
        }
    }
}

void OutputMatrix(int Matrix[MAX_ARRAY_LEN][MAX_ARRAY_LEN], int N, int M)
{
    for (int i = 0; i < N; Matrix++ && i++) {
        int* Line = *Matrix;
        for (int j = 0; j < M; Line++ && j++) {
            std::cout << *Line << " ";
        }
        std::cout << "\n";
    }
}

bool Calculate(int Matrix[MAX_ARRAY_LEN][MAX_ARRAY_LEN], int N, int M)
{
    int* LastNeg = nullptr;
    int* PreLastNeg = nullptr;
    Matrix = Matrix + N - 1;
    for (int i = N; i > 0 && (PreLastNeg == nullptr || LastNeg == nullptr); Matrix-- && i--) {
        int* Line = *Matrix + M - 1;
        for (int j = M; j > 0 && (PreLastNeg == nullptr || LastNeg == nullptr); Line-- && j--) {
            if (*Line < 0) {
                LastNeg = PreLastNeg;
                PreLastNeg = Line;
            }
        }
    }

    if (PreLastNeg == nullptr || LastNeg == nullptr || *LastNeg == *PreLastNeg) {
        return false;
    }

    int Temp = *PreLastNeg;
    *PreLastNeg = *LastNeg;
    *LastNeg = Temp;

    return true;
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");

    int Matrix[MAX_ARRAY_LEN][MAX_ARRAY_LEN];
    int N = 0;
    int M = 0;

    std::cout << "���� �������\n";
    InputMatrix(Matrix, &N, &M);
    std::cout << "��������� �������:\n";
    OutputMatrix(Matrix, N, M);

    bool result = Calculate(Matrix, N, M);
    if (!result) {
        std::cout << "���������� �� ���������� ��� ������� �� ����������\n";
        return 0;
    }

    std::cout << "���������:\n";
    OutputMatrix(Matrix, N, M);

    return 0;
}