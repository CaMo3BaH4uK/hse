/*
    * Microsoft Visual Studio 2022
    * MSVC C++ Complier (v143)
    * C++ Language Standart: ISO C++20 Standard
    *
    * Lab 8_1
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#define NOMINMAX

#include <iostream>
#include <Windows.h>
#include <limits>

#define MAX_ARRAY_LEN 255


int InputArray(int *Arr)
{
    int Count = 0;
    std::cout << "������� ���������� ��������� (> 0 � < " << MAX_ARRAY_LEN << "):\n";
    std::cin >> Count;
    while (std::cin.fail() || Count < 1 || Count > MAX_ARRAY_LEN) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "��� ���� �� �������� ����������� ���������\n";
        std::cout << "������� ���������� ��������� (> 0 � < " << MAX_ARRAY_LEN << "):\n";
        std::cin >> Count;
    }

    for (int i = 0; i < Count; Arr++ && i++) {
        std::cout << "������� ������� " << i + 1 << ":\n";
        std::cin >> *Arr;
        while (std::cin.fail()) {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "��� ���� �� �������� ���������\n";
            std::cout << "������� ������� " << i + 1 << ":\n";
            std::cin >> *Arr;
        }
    }

    return Count;
}

void OutputArray(int* Arr, int ArrLen)
{
    for (int i = 0; i < ArrLen; Arr++ && i++) {
        std::cout << *Arr << " ";
    }
    std::cout << "\n";
}

int CountOccurrences(int* Arr, int ArrLen, int Element)
{
    int Count = 0;

    for (int i = 0; i < ArrLen && Count < 2; Arr++ && i++) {
        if (*Arr == Element) {
            Count++;
        }
    }

    return Count;
}

int Calculate(int* A, int ALen, int* B, int BLen, int* C)
{
    int Count = 0;
    for (int i = 0; i < ALen; A++ && i++) {
        if (*A % 2 == 0 && CountOccurrences(B, BLen, *A)) {
            *(C++) = *A;
            Count++;
        }
    }
    return Count;
}

int main()
{
    int A[MAX_ARRAY_LEN];
    int B[MAX_ARRAY_LEN];
    int C[MAX_ARRAY_LEN];
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");

    std::cout << "���� ������� A\n";
    int ALen = InputArray(A);
    std::cout << "�������� ������ A\n";
    OutputArray(A, ALen);

    std::cout << "���� ������� B\n";
    int BLen = InputArray(B);
    std::cout << "�������� ������ B\n";
    OutputArray(B, BLen);

    int CLen = Calculate(A, ALen, B, BLen, C);

    if (!CLen) {
        std::cout << "���������� �� ����������\n";
        return 0;
    }

    std::cout << "���������:\n";
    OutputArray(C, CLen);

    return 0;
}