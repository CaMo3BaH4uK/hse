/*
    * Microsoft Visual Studio 2022
    * MSVC C Complier (v143)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 7_Add
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <Windows.h>
#include <locale.h>
#pragma endregion

#pragma region Defines
#define MAX_STRING_LEN  255

#define CLR             while (fgetc(InStream) != '\n' && !feof(InStream));

#pragma warning(disable : 4996)
#pragma warning(disable : 4703)
#pragma endregion

#pragma region Structs
struct _NODE
{
    struct _NODE* next;
    int info;
};
typedef struct _NODE NODE, * PNODE;
#pragma endregion


#pragma region FileOps

FILE* OpenFileRead()
{
    CHAR FileName[MAX_STRING_LEN];
    FILE* FileStream;
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
    while ((FileStream = fopen(FileName, "r")) == NULL) {
        printf("�� ������� ������� ���� ��� ������. ���������� ��� ���\n");
        printf("������� ��� ����� ��� ������: ");
        gets(FileName);
    }
    return FileStream;
}

FILE* OpenFileWrite()
{
    CHAR FileName[MAX_STRING_LEN];
    FILE* FileStream;
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
    while ((FileStream = fopen(FileName, "w")) == NULL) {
        printf("�� ������� �������/������� ���� ��� ������. ���������� ��� ���\n");
        printf("������� ��� ����� ��� ������: ");
        gets(FileName);
    }
    return FileStream;
}

BOOL ReadFile(
    IN  OUT FILE* InStream,
    OUT PNODE* First
)
{
    PNODE TempNode;
    while (!feof(InStream)) {
        INT Temp;
        if (fscanf(InStream, "%d", &Temp) == 1) {
            PNODE NewNode = (PNODE)malloc(sizeof(NODE));
            NewNode->info = Temp;
            NewNode->next = NULL;

            if (*First == NULL) {
                *First = NewNode;
            }
            else {
                TempNode->next = NewNode;
            }
            TempNode = NewNode;
        }
        CLR;
    }
    return *First != NULL;
}

#pragma endregion

#pragma region QueueOps

VOID WriteQueue(
    IN      FILE* OutStream,
    IN      PNODE   First
)
{
    PNODE Cur = First;
    while (Cur != NULL) {
        fprintf(OutStream, "%d\n", Cur->info);
        Cur = Cur->next;
    }
}

INT FindMin(
    IN      PNODE   First
)
{
    INT Min = First->info;
    PNODE Cur = First->next;
    while (Cur != NULL) {
        if (Cur->info < Min) {
            Min = Cur->info;
        }
        Cur = Cur->next;
    }
    return Min;
}

VOID FreeMemory(
    IN      PNODE   First,
    IN      PNODE   End
)
{
    PNODE Cur = First, ToDelete = NULL;
    while (Cur != End) {
        ToDelete = Cur;
        Cur = Cur->next;
        free(ToDelete);
    }
}

#pragma endregion

#pragma region StackOps

BOOL GenerateStackFromQueueExceptMin(
    IN      PNODE   QueueFirst,
    IN      INT     Min,
        OUT PNODE*  StackFirst
)
{
    PNODE Cur = QueueFirst;

    while (Cur != NULL) {
        if (Cur->info != Min) {
            PNODE NewNode = (PNODE)malloc(sizeof(NODE));
            NewNode->info = Cur->info;
            NewNode->next = *StackFirst;
            *StackFirst = NewNode;
        }
        Cur = Cur->next;
    }
    return *StackFirst != NULL;
}

#pragma endregion


INT main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");
    FILE* InStream = OpenFileRead();
    fseek(InStream, 0, SEEK_END);
    if (ftell(InStream) == 0) {
        printf("���� ����. ���������� ���������\n");
        return 0;
    }
    rewind(InStream);
    FILE* OutStream = OpenFileWrite();

    PNODE QueueFirst = NULL;
    PNODE StackFirst = NULL;

    if (!ReadFile(InStream, &QueueFirst)) {
        printf("� ����� �� ���� �����\n");
        return 0;
    }

    printf("�������� �������:\n");
    WriteQueue(stdout, QueueFirst);


    INT Min = FindMin(QueueFirst);

    printf("����������� ������� ����� %d\n", Min);

    if (!GenerateStackFromQueueExceptMin(QueueFirst, Min, &StackFirst)) {
        printf("������� �����\n");
        return 0;
    }

    printf("���������� ����:\n");
    WriteQueue(stdout, StackFirst);

    WriteQueue(OutStream, StackFirst);

    FreeMemory(QueueFirst, NULL);
    FreeMemory(StackFirst, NULL);
    

    return 0;
}



