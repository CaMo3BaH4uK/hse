/*
    * Microsoft Visual Studio 2022
    * MSVC C Complier (v143)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 7
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdbool.h>
#include <Windows.h>
#include <locale.h>
#pragma endregion

#pragma region Defines
#define MAX_STRING_LEN  255

#define CLR             while (fgetc(InStream) != '\n' && !feof(InStream));

#pragma warning(disable : 4996)
#pragma warning(disable : 4703)
#pragma endregion

#pragma region Structs
struct _NODE
{
    struct _NODE* next;
    int info;
};
typedef struct _NODE NODE, * PNODE;
#pragma endregion


#pragma region FileOps

FILE* OpenFileRead()
{
    CHAR FileName[MAX_STRING_LEN];
    FILE* FileStream;
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
    while ((FileStream = fopen(FileName, "r")) == NULL) {
        printf("�� ������� ������� ���� ��� ������. ���������� ��� ���\n");
        printf("������� ��� ����� ��� ������: ");
        gets(FileName);
    }
    return FileStream;
}

FILE* OpenFileWrite()
{
    CHAR FileName[MAX_STRING_LEN];
    FILE* FileStream;
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
    while ((FileStream = fopen(FileName, "w")) == NULL) {
        printf("�� ������� �������/������� ���� ��� ������. ���������� ��� ���\n");
        printf("������� ��� ����� ��� ������: ");
        gets(FileName);
    }
    return FileStream;
}

BOOL ReadFile(
    IN  OUT FILE*   InStream,
        OUT PNODE*  First
)
{
    PNODE TempNode;
    while (!feof(InStream)) {
        INT Temp;
        if (fscanf(InStream, "%d", &Temp) == 1) {
            PNODE NewNode = (PNODE)malloc(sizeof(NODE));
            NewNode->info = Temp;
            NewNode->next = NULL;

            if (*First == NULL) {
                *First = NewNode;
            }
            else {
                TempNode->next = NewNode;
            }
            TempNode = NewNode;
        }
        CLR;
    }
    return *First != NULL;
}

#pragma endregion

#pragma region QueueOps

VOID WriteQueue(
    IN      FILE*   OutStream,
    IN      PNODE   First
)
{
    PNODE Cur = First;
    while (Cur != NULL) {
        fprintf(OutStream, "%d\n", Cur->info);
        Cur = Cur->next;
    }
}

VOID FreeMemory(
    IN      PNODE   First,
    IN      PNODE   End
)
{
    PNODE Cur = First, ToDelete = NULL;
    while (Cur != End) {
        ToDelete = Cur;
        Cur = Cur->next;
        free(ToDelete);
    }
}

VOID InsertInt(
    IN  OUT PNODE*  First
)
{
    PNODE Cur = *First, Prev = NULL, Min = *First, PrevMin = NULL;

    INT A1;
    do {
        printf("������� A1: ");
        fseek(stdin, 0, SEEK_END);
    } while (scanf("%d", &A1) != 1);

    INT A2;
    do {
        printf("������� A2: ");
        fseek(stdin, 0, SEEK_END);
    } while (scanf("%d", &A2) != 1);

    PNODE A1Node = (PNODE)malloc(sizeof(NODE));
    A1Node->info = A1;
    A1Node->next = NULL;

    PNODE A2Node = (PNODE)malloc(sizeof(NODE));
    A2Node->info = A2;
    A2Node->next = NULL;

    while (Cur != NULL) {
        if (Cur->info < Min->info) {
            Min = Cur;
            PrevMin = Prev;
        }
        Prev = Cur;
        Cur = Cur->next;
    }

    A1Node->next = Min;
    A2Node->next = Min->next;
    Min->next = A2Node;

    if (PrevMin == NULL) {
        *First = A1Node;
    }
    else {
        PrevMin->next = A1Node;
    }
}

BOOL DeleteNodes(
    IN      PNODE   First
)
{
    PNODE Cur = First, FirstEven = NULL, LastEven = NULL;
    while (Cur != NULL) {
        if (Cur->info % 2 == 0) {
            if (FirstEven == NULL) {
                FirstEven = Cur;
            }
            LastEven = Cur;
        }
        Cur = Cur->next;
    }

    if (FirstEven == NULL || FirstEven == LastEven || FirstEven->next == LastEven) {
        return FALSE;
    }

    FreeMemory(FirstEven->next, LastEven);
    FirstEven->next = LastEven;
    return TRUE;

}

#pragma endregion


INT main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");
    FILE* InStream = OpenFileRead();
    fseek(InStream, 0, SEEK_END);
    if (ftell(InStream) == 0) {
        printf("���� ����. ���������� ���������\n");
        return 0;
    }
    rewind(InStream);
    FILE* OutStream = OpenFileWrite();

    PNODE First = NULL;
    
    if (!ReadFile(InStream, &First)) {
        printf("� ����� �� ���� �����\n");
        return 0;
    }

    printf("������� ����� ������� �������\n");
    WriteQueue(stdout, First);

    InsertInt(&First);
    printf("������� ����� ������� �������\n");
    WriteQueue(stdout, First);
    
    if (!DeleteNodes(First)) {
        printf("� ������� ������������ ������ ����� ��� �� ���� ��������\n");
        return 0;
    }
    printf("������� ����� �������� �������\n");
    WriteQueue(stdout, First);

    WriteQueue(OutStream, First);

    FreeMemory(First, NULL);

    return 0;
}



