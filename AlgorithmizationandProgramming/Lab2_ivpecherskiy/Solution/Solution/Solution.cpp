/*
    * Microsoft Visual Studio 2022
    * MSVC C Complier
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 2
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>
#include <stdbool.h>
#include <math.h>
#include <conio.h>
#pragma endregion

#pragma region Defines
#define MATRIX_TYPE int
#define ARRAY_TYPE int
#pragma endregion


#pragma region MAIN
signed main()
{
    setlocale(LC_ALL, "");

#pragma region Task 1
    int rows = 0, cols = 0;
    void* PointerToMatrix = NULL;
    while (PointerToMatrix == NULL)
    {
        printf_s("������� ���-�� ����� � �������: ");
        char term;
        if (scanf_s("%d%c", &rows, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || rows <= 0)
        {
            rows = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("��� ���� �� ����� �������� ���-��� ����� � �������\n");
        }
        else
        {
            PointerToMatrix = malloc(rows * sizeof(MATRIX_TYPE*));
            if (PointerToMatrix == NULL)
            {
                rows = 0;
                printf_s("���������� �������� ����������� ���������� ������ ��� �������� ���������� �� ������\n");
            }
        }
    }
    MATRIX_TYPE** matrix = (MATRIX_TYPE**)PointerToMatrix;

    while (cols <= 0)
    {
        printf_s("������� ���-�� �������� � �������: ");
        char term;
        if (scanf_s("%d%c", &cols, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || cols <= 0)
        {
            cols = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("��� ���� �� ����� �������� ���-��� �������� � �������\n");
        }
        else
        {
            int i = 0;
            bool FailedAllocation = false;
            while (i < rows && FailedAllocation == false)
            {
                void* PointerToRow = malloc(cols * sizeof(MATRIX_TYPE));
                if (PointerToRow == NULL)
                {
                    for (int j = i - 1; j >= 0; j--)
                    {
                        free((void*)matrix[j]);
                    }
                    FailedAllocation = true;
                    cols = 0;
                    printf_s("���������� �������� ����������� ���������� ������ ��� �������� ������\n");
                }
                else
                {
                    matrix[i] = (MATRIX_TYPE*)PointerToRow;
                }
                i++;
            }
        }
    }


    int count = 0;
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            int element = 0;
            bool CheckPassed = false;
            while (!CheckPassed)
            {
                printf_s("������� ������� ������ %d %d: ", i, j);
                char term;
                if (scanf_s("%d%c", &element, &term) != 2 || (term != '\n' && term != ' ' && term != '\t'))
                {
                    element = 0;
                    fseek(stdin, 0, SEEK_END);
                    printf_s("��� ���� �� ����� �������� ��������� ������ � �������\n");
                }
                else
                {
                    CheckPassed = true;
                }
            }
            matrix[i][j] = element;
        }
    }

    printf_s("��������� �������: \n");
    for (int i = 0; i < rows; i++)
    {
        for (int j = 0; j < cols; j++)
        {
            printf("%d\t", matrix[i][j]);
        }
        printf_s("\n");
    }
    printf_s("\n");

    int StartedRows = rows;
    for (int i = 0; i < rows; i++)
    {
        int j = 0;
        bool FoundedOdd = false;
        while (j < cols && FoundedOdd == false)
        {
            if (matrix[i][j] % 2 != 0)
            {
                FoundedOdd = true;
            }
            j++;
        }
        if (FoundedOdd == false)
        {
            rows--;
            for (int j = i; j < rows; j++)
            {
                matrix[j] = matrix[j + 1];
            }
            i--;
        }
    }

    if (rows == StartedRows)
    {
        printf_s("�������� �� ���������\n");
    }
    else if (rows == 0)
    {
        printf_s("������� ��������� �������\n");
        return 0;
    }
    else
    {
        PointerToMatrix = realloc(PointerToMatrix, rows * sizeof(MATRIX_TYPE*));
        if (PointerToMatrix == NULL)
        {
            printf_s("���������� �������� ����������� ���������� ������ ��� �������� ���������� �� ������\n ���������� ���������� ��������� ����������\n");
            return 0;
        }

        printf_s("������� �������: \n");
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                printf("%d\t", matrix[i][j]);
            }
            printf_s("\n");
        }
    }
#pragma endregion

    _getch();
    return 0;
}
#pragma endregion