/*
    * Microsoft Visual Studio 2022
    * MSVC C Complier
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 2
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>
#include <stdbool.h>
#include <math.h>
#include <conio.h>
#pragma endregion

#pragma region Defines
#define MATRIX_TYPE int
#define ARRAY_TYPE int
#pragma endregion


#pragma region MAIN
signed main()
{
    setlocale(LC_ALL, "");

#pragma region Task 2
    void* PointerToArray = NULL;
    void* PointerToNewArray = NULL;
    int ArrayLen = 0;
    while (PointerToArray == NULL || PointerToNewArray == NULL)
    {
        printf_s("������� ����� �������: ");
        char term;
        if (scanf_s("%d%c", &ArrayLen, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || ArrayLen <= 0)
        {
            ArrayLen = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("��� ���� �� ����� �������� ������ �������\n");
        }
        else
        {
            PointerToArray = malloc(ArrayLen * sizeof(ARRAY_TYPE));
            if (PointerToArray == NULL)
            {
                printf_s("���������� �������� ����������� ���������� ������ ��� �������� �������\n");
            }
            else
            {
                PointerToNewArray = malloc(ArrayLen * sizeof(ARRAY_TYPE));
                if (PointerToNewArray == NULL)
                {
                    free(PointerToArray);
                    printf_s("���������� �������� ����������� ���������� ������ ��� �������� �������\n");
                }
            }
        }
    }
    ARRAY_TYPE* array = (ARRAY_TYPE*)PointerToArray;
    ARRAY_TYPE* newarray = (ARRAY_TYPE*)PointerToNewArray;

    for (int i = 0; i < ArrayLen; i++)
    {
        int element = 0;
        while (element < 1)
        {
            printf_s("������� ������� ������� %d: ", i);
            char term;
            if (scanf_s("%d%c", &element, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || element < 0)
            {
                element = 0;
                fseek(stdin, 0, SEEK_END);
                printf_s("��� ���� �� ����� �������� ��������� ������ � �������\n");
            }
        }
        array[i] = element;
    }

    printf_s("��������� ������: ");
    for (int i = 0; i < ArrayLen; i++)
    {
        printf_s("%d ", array[i]);
    }
    printf_s("\n");

    for (int i = 0; i < ArrayLen; i++)
    {
        int element = array[i];
        int sum = 0;
        while (element > 0)
        {
            sum += element % 10;
            element /= 10;
        }
        newarray[i] = sum;
    }

    printf_s("�������� ������: ");
    for (int i = 0; i < ArrayLen; i++)
    {
        printf_s("%d ", newarray[i]);
    }

#pragma endregion

    _getch();
    return 0;
}
#pragma endregion