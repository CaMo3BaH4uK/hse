/*
	* Microsoft Visual Studio 2022
	* MSVC C++ Complier (v143)
	* C++ Language Standart: ISO C++20 Standard
	*
	* KR4
	* Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
	* x86_64 platform
	* 
	* 15: Сформировать линейный однонаправленный стек. Вставить ноль после каждого максимального элемента.
*/

#define NOMINMAX

#include <iostream>
#include <limits>
#include <fstream>
#include <string>
#include <windows.h>

typedef struct _NODE {
	int data = 0;
	struct _NODE* link = nullptr;
} NODE, * PNODE;

int readFileAndFindMax(std::string filename, PNODE& stackTop)
{
	std::string data;
	std::fstream f;
	int maxData = 0; // Избавление от C6001. Значение инициализации НЕ влияет на результаты поиска.

	f.open(filename);
	if (!f) {
		std::cout << "Файл не найдён" << std::endl;
		exit(1);
	}
	else if (f.peek() == EOF) {
		std::cout << "Файл пуст" << std::endl;
		exit(1);
	}
	else {
		while (!f.eof())
		{
			PNODE stackCur = new NODE;
			getline(f, data);
			stackCur->data = stoi(data);
			stackCur->link = stackTop;
			if (!stackTop or stackCur->data > maxData)
			{
				maxData = stackCur->data;
			}
			stackTop = stackCur;
		}
	}
	f.close();
	return maxData;
}

void printList(PNODE stackTop)
{
	PNODE stackCur = stackTop;
	while (stackCur)
	{
		std::cout << stackCur->data << std::endl;
		stackCur = stackCur->link;
	}
}

void writeFile(std::string filename, PNODE stackTop)
{
	std::fstream f;
	f.open(filename, std::fstream::out);
	PNODE stackCur = stackTop;
	while (stackCur)
	{
		f << stackCur->data << std::endl;
		stackCur = stackCur->link;
	}
	f.close();
}

void insertZeros(PNODE stackTop, int maxData) {
	PNODE stackCur = stackTop;
	while (stackCur)
	{
		if (stackCur->data == maxData)
		{
			PNODE insertNode = new NODE;
			insertNode->link = stackCur->link;
			stackCur->link = insertNode;
			// stackTop не нужно изменять, так как мы вставляем после
			stackCur = insertNode; // Пропускаем insertNode, так как maxData может быть равен 0. В таком случае получим бесконечный цикл.
		}
		stackCur = stackCur->link;
	}
}

void freeMemory(PNODE stackTop) {
	PNODE stackCur = stackTop;
	PNODE stackNext = nullptr;
	while (stackCur)
	{
		stackNext = stackCur->link;
		delete stackCur;
		stackCur = stackNext;
	}
}

int main()
{
	setlocale(LC_ALL, "ru_ru.utf8");
	SetConsoleCP(65001);
	SetConsoleOutputCP(65001);

	PNODE stackTop = nullptr;
	std::string filenameIn, filenameOut;
	std::cout << "Введите имя входного файла: ";
	std::cin >> filenameIn;
	std::cout << "Введите имя выходного файла: ";
	std::cin >> filenameOut;

	int maxData = readFileAndFindMax(filenameIn, stackTop);
	std::cout << "Максимальный элемент " << maxData << ". Исходный список:" << std::endl;
	printList(stackTop);

	insertZeros(stackTop, maxData);
	std::cout << "Изменённый список:" << std::endl;
	printList(stackTop);

	writeFile(filenameOut, stackTop);

	freeMemory(stackTop);

	return 0;
}