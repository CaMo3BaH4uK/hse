/*
    * Microsoft Visual Studio 2019
    * MSVC C Complier (v142)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * KR 1_2
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * Windows x86_32 platform

    2.	Дана матрица В[0:N-1,0:N-1] вещественного типа. Написать программу, которая
    обнуляет отрицательные элементы матрицы, расположенные выше главной диагонали,
    если произведение элементов первого столбца положительное. Вычисления оформить
    в виде функции с параметрами.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <locale.h>
#include <math.h>
#include <windows.h>

#define LMAX 50

bool calculate(float matrix[LMAX][LMAX], int n) {
    bool changed = false;
    float mul = 1.0;
    for (int i = 0; i < n; i++)
    {
        mul *= matrix[i][0];
    }
    if (mul > 0)
    {
        for (int i = 0; i < n; i++)
        {
            for (int j = i+1; j < n; j++)
            {
                if (matrix[i][j] < 0)
                {
                    changed = true;
                    matrix[i][j] = 0;
                }
            }
        }
    }
    return changed;
}

int main()
{
    SetConsoleOutputCP(65001);

    float matrix[LMAX][LMAX];
    int n = 0, m = 0;

    while (!n)
    {
        char term;
        printf_s("Введите N (0 < N <= %d): ", LMAX);
        if (scanf_s("%d%c", &n, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || n < 1 || n > LMAX)
        {
            n = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Попробуйте ещё раз\n");
        }
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            matrix[i][j] = 0;
            while (!matrix[i][j])
            {
                char term;
                printf_s("Введите элемент матрицы %d %d (соблюдая настройки региона в операционной системе): ", j, i);
                if (scanf_s("%f%c", &matrix[i][j], &term) != 2 || (term != '\n' && term != ' ' && term != '\t'))
                {
                    matrix[i][j] = 0;
                    fseek(stdin, 0, SEEK_END);
                    printf_s("Попробуйте ещё раз\n");
                }
            }
        }
    }

    printf("Введённая матрица:\n");
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < n; j++)
        {
            printf("%f\t", matrix[i][j]);
        }
        printf("\n");
    }

    bool changed = calculate(matrix, n);

    if (!changed)
    {
        printf("Матрица не изменилась\n");
    }
    else
    {
        printf("Полученная матрица:\n");
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                printf("%f\t", matrix[i][j]);
            }
            printf("\n");
        }
    }
}