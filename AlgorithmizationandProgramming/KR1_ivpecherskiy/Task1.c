/*
    * Microsoft Visual Studio 2019
    * MSVC C Complier (v142)
    * C Language Standart: ISO C17 (2018) Standard
    * 
    * KR 1_1
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * Windows x86_32 platform
     
    1.	Даны целочисленная матрица С[0:N-1,0:M-1] и целочисленный массив D[0:K-1]. 
    Написать программу, которая сортирует по возрастанию методом установки элементы 
    тех строк матрицы С, все элементы которых отсутствуют в массиве D. Вычисления 
    оформить в виде функции с параметрами.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <locale.h>
#include <math.h>
#include <windows.h>

#define LMAX 50

bool calculate(int matrix[LMAX][LMAX] , int arr[LMAX], int n, int m, int k) {
    bool changed = false;
    for (int i = 0; i < n; i++)
    {
        int j = 0;
        int l = k;
        while (l == k && j < m)
        {
            l = 0;
            while (matrix[i][j] != arr[l] && l < k) l++;
            j++;
        }
        if (j == m && l == k)
        {
            changed = true;
            int a, b;
            for (int a = 0; a < j - 1; a++)
            {
                for (int b = 0; b < j - a - 1; b++)
                {
                    if (matrix[i][b] > matrix[i][b + 1])
                    {
                        int temp = matrix[i][b + 1];
                        matrix[i][b + 1] = matrix[i][b];
                        matrix[i][b] = temp;
                    }
                }
            }
        }
    }
    return changed;
}

int main()
{
    SetConsoleOutputCP(65001);

    int matrix[LMAX][LMAX], arr[LMAX];
    int n = 0, m = 0, k = 0;

    while (!n)
    {
        char term;
        printf_s("Введите N (0 < N <= %d): ", LMAX);
        if (scanf_s("%d%c", &n, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || n < 1 || n > LMAX)
        {
            n = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Попробуйте ещё раз\n");
        }
    }

    while (!m)
    {
        char term;
        printf_s("Введите M (0 < M <= %d): ", LMAX);
        if (scanf_s("%d%c", &m, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || m < 1 || m > LMAX)
        {
            m = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Попробуйте ещё раз\n");
        }
    }

    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            matrix[i][j] = 0;
            while (!matrix[i][j])
            {
                char term;
                printf_s("Введите элемент матрицы %d %d (число должно быть целое): ", j, i);
                if (scanf_s("%d%c", &matrix[i][j], &term) != 2 || (term != '\n' && term != ' ' && term != '\t'))
                {
                    matrix[i][j] = 0;
                    fseek(stdin, 0, SEEK_END);
                    printf_s("Попробуйте ещё раз\n");
                }
            }
        }
    }

    printf("Введённая матрица:\n");
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            printf("%d\t", matrix[i][j]);
        }
        printf("\n");
    }

    while (!k)
    {
        char term;
        printf_s("Введите K (0 < K <= %d): ", LMAX);
        if (scanf_s("%d%c", &k, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || k < 1 || k > LMAX)
        {
            k = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Попробуйте ещё раз\n");
        }
    }

    for (int i = 0; i < k; i++)
    {
        arr[i] = 0;
        while (!arr[i])
        {
            char term;
            printf_s("Введите элемент массива %d (число должно быть целое): ", i);
            if (scanf_s("%d%c", &arr[i], &term) != 2 || (term != '\n' && term != ' ' && term != '\t'))
            {
                arr[i] = 0;
                fseek(stdin, 0, SEEK_END);
                printf_s("Попробуйте ещё раз\n");
            }
        }
    }

    printf("Введённый массив:\n");
    for (int i = 0; i < k; i++)
    {
        printf("%d\t", arr[i]);
    }
    printf("\n");

    bool changed = calculate(matrix, arr, n, m, k);

    if (!changed)
    {
        printf("Матрица не сортировалась\n");
    }
    else
    {
        printf("Полученная матрица:\n");
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                printf("%d\t", matrix[i][j]);
            }
            printf("\n");
        }
    }
}