/*
    * Microsoft Visual Studio 2019
    * MSVC C Complier (v142)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 4
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <Windows.h>
#include <locale.h>
#pragma endregion

#pragma region Defines
#define STRS_BUFFER_SIZE 100
#define STR_MAX_LEN 100
#define SUBSTR_BUFFER_SIZE 200
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)
#pragma endregion

#pragma region Types
typedef struct substr {
    char* pointer;
    int len;
    char str[STR_MAX_LEN];
} substr;
#pragma endregion

#pragma region Functions
int StrSplitter(char instrings[STRS_BUFFER_SIZE][STR_MAX_LEN], substr substrings[SUBSTR_BUFFER_SIZE], int n);
substr* FindLongestString(substr substrings[SUBSTR_BUFFER_SIZE], int substringsnumber);
char* InsertDots(char* string);
#pragma endregion


int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");
    char instrings[STRS_BUFFER_SIZE][STR_MAX_LEN];
    substr substrings[SUBSTR_BUFFER_SIZE];
    
#pragma region Input
    int n = 0;
    while (!n)
    {
        char term;
        printf("������� ���������� �����: ");
        if (scanf_s("%d%c", &n, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || n < 1)
        {
            n = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("���������� ��� ���\n");
        }
    }


    for (int i = 0; i < n; i++)
    {
        printf("������� ������: ");
        gets(instrings[i]);
    }
#pragma endregion

#pragma region Task 1
    int substringsnumber = StrSplitter(instrings, substrings, n);
    if (!substringsnumber)
    {
        printf("�������� �� ����������");
        return 0;
    }
    printf("��������� ���������:\n");
    for (int i = 0; i < substringsnumber; i++)
    {
        puts(substrings[i].str);
    }
#pragma endregion

#pragma region Task 2
    substr* maximum = FindLongestString(substrings, substringsnumber);
    printf("����� ������������ ������ ����� %d \n������: ", (*maximum).len);
    puts((*maximum).str);
#pragma endregion

#pragma region Task 3
    char* result = InsertDots((*maximum).pointer);
    if (result == NULL)
    {
        printf("����� �� �������\n");
        return 0;
    }

    printf("���������� ������: ");
    puts(result);
#pragma endregion

    return 0;
}

int StrSplitter(char instrings[STRS_BUFFER_SIZE][STR_MAX_LEN], substr substrings[SUBSTR_BUFFER_SIZE], int n)
{
    int substringsnumber = 0;
    while (n)
    {
        char* str = *instrings;
        char* start = *instrings - 1;
        while (*(str-1) != 0)
        {
            if ((*str == '.' || *str == ',' || *str == ';' || *str == ':' || *str == 0) && str - start > 1)
            {
                substrings[substringsnumber].pointer = *instrings;
                substrings[substringsnumber].len = str - start - 1;
                strncpy(substrings[substringsnumber].str, start + 1, str - start - 1);
                substrings[substringsnumber].str[str - start - 1] = 0;
                start = str;
                substringsnumber++;
            }
            else if (*str == '.' || *str == ',' || *str == ';' || *str == ':')
            {
                start = str;
                str++;
            }
            else
            {
                str++;
            }
            
        }
        instrings++;
        n--;
    }
    return substringsnumber;
}

substr* FindLongestString(substr substrings[SUBSTR_BUFFER_SIZE], int substringsnumber) {
    substr* maximum = substrings;
    for (substr* substring = substrings + 1; substring < substrings + substringsnumber; substring++)
    {
        if ((*substring).len > (*maximum).len)
        {
            maximum = substring;
        }
    }
    return maximum;
}

char* InsertDots(char* string)
{
    bool inserted = false;
    char* chr = string;
    int len = strlen(string);
    while (!inserted && *chr != 0)
    {
        if (isdigit(*chr))
        {
            for (char* new_chr = string + len + 3; new_chr > chr + 3; new_chr--)
            {
                *new_chr = *(new_chr - 3);
            }
            for (char* new_chr = chr + 1; new_chr < chr + 4; new_chr++)
            {
                *new_chr = '.';
            }
            inserted = true;
        }
        chr++;
    }
    if (!inserted)
    {
        return NULL;
    }
    return string;
}