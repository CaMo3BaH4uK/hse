/*
    * Microsoft Visual Studio 2022
    * MSVC C Complier
    * C Language Standart: ISO C17 (2018) Standard
    * 
    * Lab 1
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>
#include <stdbool.h>
#include <math.h>
#include <conio.h>
#pragma endregion

#pragma region MAIN
int main()
{
    setlocale(LC_ALL, "");
    printf_s("������������ ������ 1\n");
#pragma region Task 1
    printf_s("������� 1\n");

    int32_t ArrayLen = 0;
    void* PointerToArray = NULL;
    while (ArrayLen <= 0)
    {
        printf_s("������� ����� ������� R �� 1\n");
        char term;
        
        if (scanf_s("%d%c", &ArrayLen, &term) == 2 && (term == '\n' || term == ' ' || term == '\t'))
        {
            ArrayLen++;
            PointerToArray = calloc(ArrayLen, sizeof(float));
            if (PointerToArray == NULL)
            {
                printf_s("�� ������� �������� ���������� ������������ ��� �������.\n���������� ����� ������ ������.\n");
                ArrayLen = 0;
            }
        }
        else
        {
            ArrayLen = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("�������� ����� �� �������� ��� ����������\n");
        }
    }

    float x, h, a;
    printf_s("������� x\n");
    scanf_s("%f", &x);
    printf_s("������� h\n");
    scanf_s("%f", &h);
    printf_s("������� a\n");
    scanf_s("%f", &a);


    float* R = (float*)PointerToArray;
    for (uint32_t i = 1; i < ArrayLen; i++)
    {
        R[i] = 0.8 * cos(8 * a * x - i * h);
    }

    printf_s("������ R �� %d ���������:\n", ArrayLen - 1);
    for (uint32_t i = 1; i < ArrayLen; i++)
    {
        printf_s("%f ", R[i]);
    }
    printf_s("\n");
#pragma endregion

#pragma region Additional task
    float eps = 0.0;
    printf_s("������� �������� (eps)\n");
    scanf_s("%f", &eps);

    uint32_t MaximumElementPosition = 1;
    uint32_t MinumumElementPosition = 1;

    for (uint32_t i = 2; i < ArrayLen; i++)
    {
        if (R[i] < R[MinumumElementPosition])
        {
            MinumumElementPosition = i;
        }
        if (R[i] > R[MaximumElementPosition])
        {
            MaximumElementPosition = i;
        }
    }

    if (MaximumElementPosition == 1 && MinumumElementPosition == 1)
    {
        printf_s("��� �������� �����\n");
        return 0;
    }

    float MinimumElement = R[MinumumElementPosition];
    float MaximumElement = R[MaximumElementPosition];

    printf_s("����������� ������� %f, ������������ ������� %f\n", MinimumElement, MaximumElement);

    int32_t MaximumCounter = 0;
    for (uint32_t i = 1; i < ArrayLen; i++)
    {
        if (fabs(MaximumElement - R[i]) < eps)
        {
            MaximumCounter++;
            printf_s("������ �������� %f\n", R[i]);
        }
    }

    PointerToArray = realloc(PointerToArray, sizeof(float) * (ArrayLen + MaximumCounter));
    ArrayLen += MaximumCounter;
    if (PointerToArray == NULL)
    {
        printf_s("�� ������� ���������������� ������ ��� ������ �������.\n");
        return 0;
    }
    R = (float*)PointerToArray;

    uint32_t i = 1;
    while (MaximumCounter > 0)
    {
        if (fabs(MaximumElement - R[i]) < eps)
        {
            i++;
            MaximumCounter--;
            for (int32_t j = ArrayLen - 1; j > i; j--)
            {
                R[j] = R[j - 1];
            }
            R[i] = MinimumElement;
        }
        i++;
    }

    printf_s("������ R �� %d ���������\n", ArrayLen - 1);
    for (uint32_t i = 1; i < ArrayLen; i++)
    {
        printf_s("%f ", R[i]);
    }
#pragma endregion

//#pragma region Task 2
//    printf_s("������� 2\n");
//    uint32_t NegativeElementPosition = 0;
//    for (uint32_t i = 1; i < ArrayLen; i++)
//    {
//        if (R[i] < 0)
//        {
//            NegativeElementPosition = i;
//            break;
//        }
//    }
//    if (NegativeElementPosition == 0)
//    {
//        printf_s("������������� ������� �� ������\n��� �������� ��������\n");
//        free(PointerToArray);
//        return 0;
//    }
//
//    uint32_t Position = NegativeElementPosition+1;
//    for (uint32_t i = NegativeElementPosition+1; i < ArrayLen; i++)
//    {
//        if (R[i] <= 0)
//        {
//            R[Position] = R[i];
//            Position++;
//        }
//    }
//
//    if (ArrayLen == Position)
//    {
//        printf_s("�������� �� ���������\n");
//    }
//
//    ArrayLen = Position;
//    printf_s("������ R �� %d ���������\n", ArrayLen - 1);
//    for (uint32_t i = 1; i < ArrayLen; i++)
//    {
//        printf_s("%f ", R[i]);
//    }
//    printf_s("\n");
//#pragma endregion
//
//#pragma region Task 3
//    printf_s("������� 3\n");
//    uint32_t MinumumElementPosition = 1;
//    for (uint32_t i = 1; i < ArrayLen; i++)
//    {
//        if (R[i] < R[MinumumElementPosition])
//        {
//            MinumumElementPosition = i;
//        }
//    }
//    if (abs(NegativeElementPosition-MinumumElementPosition) < 2)
//    {
//        printf_s("������ ������������� � ������ ����������� �������� ��������� ��� ��������� �����\n��� �������� ��������\n");
//        free(PointerToArray);
//        return 0;
//    }
//
//    uint32_t StartPoint = min(NegativeElementPosition, MinumumElementPosition)+1;
//    uint32_t StopPoint = max(NegativeElementPosition, MinumumElementPosition);
//    float Sum = 0;
//    for (uint32_t i = StartPoint; i < StopPoint; i++)
//    {
//        Sum += R[i];
//    }
//    float Avg = Sum / (StopPoint - StartPoint);
//    printf_s("������� �������������� ����� %f\n", Avg);
//#pragma endregion


    printf_s("\n����� ���������\n");
    _getch();
    return 0;
}
#pragma endregion