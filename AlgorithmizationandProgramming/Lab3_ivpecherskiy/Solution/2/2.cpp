/*
    * Microsoft Visual Studio 2019
    * MSVC C Complier (v142)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 3_2
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>
#include <stdbool.h>
#include <math.h>
#include <conio.h>
#include <windows.h>
#pragma endregion

#pragma region Defines
#define MATRIX_TYPE int
#pragma endregion

#pragma region Functions
bool AllocateAndInput(int* rows, int* cols, MATRIX_TYPE*** matrix);
int calculation(int rows, int cols, MATRIX_TYPE** matrix);
#pragma endregion

signed main()
{
    SetConsoleOutputCP(65001);
    setlocale(LC_ALL, "ru_RU.utf8");
    int rows = 0, cols = 0;
    MATRIX_TYPE** matrix = NULL;
    while(!AllocateAndInput(&rows, &cols, &matrix));
    MATRIX_TYPE result = calculation(rows, cols, matrix);
    printf_s("Результат %d", result);
    return 0;
}

bool AllocateAndInput(int* rows, int* cols, MATRIX_TYPE*** matrix)
{
    while (*matrix == NULL)
    {
        printf_s("Введите количество строк в матрице ( > 1 ): ");
        char term;
        if (scanf_s("%d%c", rows, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || *rows <= 1)
        {
            *rows = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Ваш ввод не может являться количеством строк в матрице\n");
        }
        else
        {
            *matrix = (MATRIX_TYPE**)malloc(*rows * sizeof(MATRIX_TYPE*));
            if (*matrix == NULL)
            {
                *rows = 0;
                printf_s("Невозможно выделить достаточное количество памяти для хранения указателей.\nПопробуйте другое число\n");
            }
        }
    }

    while (*cols <= 0)
    {
        printf_s("Введите количество столбцов в матрице ( > 1 ): ");
        char term;
        if (scanf_s("%d%c", cols, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || *cols <= 1)
        {
            *cols = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Ваш ввод не может являться количеством столбцов в матрице\n");
        }
        else
        {
            int i = 0;
            while (i < *rows)
            {
                *(*matrix + i) = (MATRIX_TYPE*)malloc(*cols * sizeof(MATRIX_TYPE));
                if (*(*matrix + i) == NULL)
                {
                    for (int j = i - 1; j >= 0; j--)
                    {
                        free((void*)(*(*matrix + j)));
                        (void*)(*(*matrix + j)) = NULL;
                    }
                    *cols = 0;
                    free((void *)(*matrix));
                    *matrix = NULL;
                    printf_s("Невозможно выделить достаточно памяти для хранения.\n Попробуйте снова задать матрицу\n");
                    return false;
                }
                i++;
            }
        }
    }

    for (MATRIX_TYPE** i = *matrix; i < *matrix + *rows; i++)
    {
        for (MATRIX_TYPE* j = *i; j < *i + *cols; j++)
        {
            char term;
            printf_s("Введите элемент матрицы %d %d: ", j - *i, i - *matrix);
            while (scanf_s("%d%c", j, &term) != 2 || (term != '\n' && term != ' ' && term != '\t'))
            {
                fseek(stdin, 0, SEEK_END);
                printf_s("Ваш ввод не может являться элементом матрицы\n");
                *j = 0;
                printf_s("Введите элемент матрицы %d %d: ", j - *i, i - *matrix);
            }
        }
    }

    printf_s("Введённая матрица: \n");
    for (MATRIX_TYPE** i = *matrix; i < *matrix + *rows; i++)
    {
        for (MATRIX_TYPE* j = *i; j < *i + *cols; j++)
        {
            printf("%d\t", *j);
        }
        printf_s("\n");
    }
    printf_s("\n");

    return true;
}

MATRIX_TYPE calculation(int rows, int cols, MATRIX_TYPE** matrix)
{
    MATRIX_TYPE max_element = abs(**matrix - *(*matrix + 1));
    for (MATRIX_TYPE** i = matrix; i < matrix + rows; i++)
    {
        MATRIX_TYPE min_element = abs(**i - *(*i + 1));
        for (MATRIX_TYPE* j = *i + 1; j < *i + cols - 1; j++)
        {
            if (abs(*j - *(j + 1)) < min_element)
            {
                min_element = abs(*j - *(j + 1));
            }
        }
        if (min_element > max_element)
        {
            max_element = min_element;
        }
    }
    return max_element;
}
