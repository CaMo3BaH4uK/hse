#include <stdio.h>
#include <locale.h>
#include <windows.h>
#define lmax 10
void input_array(int* n, int a[])
{
    SetConsoleOutputCP(65001);
    setlocale(LC_ALL, "ru_RU.utf8");
    int i, k;
    printf("Введите n\n");
    do
    {
        k = scanf("%d", n);
        while (getchar() != '\n');
    } while (*n <= 0 || *n >= lmax || k != 1);
    printf("Введите массив из %d символов\n", *n);
    for (i = 0; i < *n; i++)
        scanf("%d", &a[i]);
}
void print_array(int n, int a[])
{
    int i;
    for (i = 0; i < n; i++)
        printf("%5d", *(a + i));
    printf("\n");
}
int edin(int a[], int na)
{
    int col = 0;
    for (int* i = a; i < na + a; i++)
    {
        int num = *i;
        int flag = 1;
        while (num > 0 && flag)
        {
            if (num % 10 == 1)
            {
                flag = 0;
                col++;
            }
            num = num / 10;
        }
    }
    return col;
}
int delete_it(int a[], int na)
{
    for (int* i = a; i < na + a; i++)
    {
        int flag = 0;
        for (int* j = i + 1; j < na + a; j++)
        {
            if (*j == *i)
            {
                flag = 1;
                for (int* l = j; l < na + a - 1; l++)
                {
                    *l = *(l + 1);
                }
                na--;
                j--;
            }
        }
        if (flag)
        {
            for (int* l = i; l < na + a - 1; l++)
            {
                *l = *(l + 1);
            }
            na--;
            i--;
        }
    }
    return na;
}
int main()
{
    setlocale(LC_ALL, "Rus");
    int a[lmax], b[lmax], kol[lmax], na, nb, nd;
    input_array(&na, a);
    nb = edin(a, na);
    printf("Массив a : ");
    print_array(na, a);
    printf("Элементов с 1 в числе %d\n", nb);
    nd = delete_it(a, na);
    printf("Конечный массив: ");
    print_array(nd, a);
    return 0;
}
