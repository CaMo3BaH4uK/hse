/*
    * Microsoft Visual Studio 2019
    * MSVC C Complier (v142)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 3_1
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>
#include <stdbool.h>
#include <math.h>
#include <conio.h>
#include <windows.h>
#pragma endregion


#pragma region Functions
bool dataInput(int* n, int* k);
float factorial(int n);
float combinations(int n, int k);
#pragma endregion

int main()
{
    SetConsoleOutputCP(65001);
    setlocale(LC_ALL, "ru_RU.utf8");
    int n = 0, k = 0;
    dataInput(&n, &k);
    printf_s("Результат: %d", (int)combinations(n, k));
}

bool dataInput(int* n, int* k)
{
    while (!*n)
    {
        char term;
        printf_s("Введите n (n >= 0): ");
        if (scanf_s("%d%c", n, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || *n < 1)
        {
            *n = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Попробуйте ещё раз\n");
        }
    }
    while (!*k)
    {
        char term;
        printf_s("Введите k (n >= k): ");
        if (scanf_s("%d%c", k, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || *k > *n || *k < 0)
        {
            *k = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Попробуйте ещё раз\n");
        }
    }
    return true;
}

float factorial(int n) {
    if (n == 0)
        return 1;
    else return factorial(n - 1) * n;
}

float combinations(int n, int k)
{
    return factorial(n)/(factorial(k)*factorial(n-k));
}
