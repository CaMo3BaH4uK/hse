/*
    * Microsoft Visual Studio 2019
    * MSVC C Complier (v142)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 3_Add
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>
#include <stdbool.h>
#include <math.h>
#include <conio.h>
#include <windows.h>
#pragma endregion

#pragma region Defines
#define ARRAY_TYPE int
#pragma endregion

#pragma region Types
#pragma endregion

#pragma region Functions
bool dataInput(ARRAY_TYPE** array_in, int* len);
bool firstTask(ARRAY_TYPE** array_in, int len);
bool secondTask(ARRAY_TYPE** array_in, int* len);
bool printArray(ARRAY_TYPE** array_in, int len, int task);
#pragma endregion

signed main()
{
    SetConsoleOutputCP(65001);
    setlocale(LC_ALL, "ru_RU.utf8");
    ARRAY_TYPE* array_in = NULL;
    int len = 0;
    dataInput(&array_in, &len);
    firstTask(&array_in, len);
    printArray(&array_in, len, 1);
    secondTask(&array_in, &len);
    printArray(&array_in, len, 2);
    return 0;
}

bool dataInput(ARRAY_TYPE** array_in, int* len)
{
    printf_s("Введите длину массива: ");
    char term;
    while (*len == 0)
    {
        if (scanf_s("%d%c", len, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || *len <= 0)
        {
            *len = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Ваш ввод не может являться длиной массива\n");
            printf_s("Введите длину массива: ");
        }
        else
        {
            *array_in = (ARRAY_TYPE*)malloc(*len * sizeof(ARRAY_TYPE));
            if (*array_in == NULL)
            {
                *len = 0;
                printf_s("Невозможно выделить достаточное количество памяти для хранения указателей.\nПопробуйте другое число\n");
            }
        }
    }

    for (ARRAY_TYPE* i = *array_in; i < *array_in + *len; i++)
    {
        char term;
        printf_s("Введите элемент массива %d: ", i - *array_in);
        while (scanf_s("%d%c", i, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || *i < 0)
        {
            fseek(stdin, 0, SEEK_END);
            printf_s("Ваш ввод не может являться элементом матрицы\n");
            printf_s("Введите элемент массива %d: ", i - *array_in);
        }
    }
    printf_s("Исходный массив:\n");
    for (ARRAY_TYPE* i = *array_in; i < *array_in + *len; i++)
    {
        printf_s("%d ", *i);
    }
    printf_s("\n");
    return true;
}

bool firstTask(ARRAY_TYPE** array_in, int len)
{
    for (ARRAY_TYPE* i = *array_in; i < len + *array_in; i++)
    {
        int negative = *i < 0 ? -1 : 1;
        ARRAY_TYPE num = *i * negative;
        int num_len = 1;
        int last = num % 10;
        num = num / 10;
        int first = 0;
        while (num > 0)
        {
            first = num % 10;
            num = num / 10;
            num_len++;
        }
        if (num_len > 1)
        {
            *i = (*i * negative - first * pow(10, num_len - 1) - last + last * pow(10, num_len - 1) + first)*negative;
        }
    }
    return true;
}

bool secondTask(ARRAY_TYPE** array_in, int* len)
{
    for (ARRAY_TYPE* i = *array_in; i < *len + *array_in; i++)
    {
        if ((*i)%2 != 0)
        {
            for (ARRAY_TYPE* j = i; j < *len + *array_in - 1; j++)
            {
                *j = *(j + 1);
            }
            i--;
            (*len)--;
        }
    }
    return true;
}

bool printArray(ARRAY_TYPE** array_in, int len, int task)
{
    printf("Результат функции номер %d:\n", task);
    for (ARRAY_TYPE* i = *array_in; i < len + *array_in; i++)
    {
        printf_s("%d ", *i);
    }
    printf_s("\n");
    return true;
}
