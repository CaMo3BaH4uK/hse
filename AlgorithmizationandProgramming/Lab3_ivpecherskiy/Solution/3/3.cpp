/*
    * Microsoft Visual Studio 2019
    * MSVC C Complier (v142)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 3_3
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <locale.h>
#include <stdbool.h>
#include <math.h>
#include <conio.h>
#include <windows.h>
#pragma endregion

#pragma region Defines
#define ARRAY_TYPE int
#pragma endregion

#pragma region Types
typedef struct repeats {
    ARRAY_TYPE number;
    int count;
} repeats;
#pragma endregion

#pragma region Functions
bool dataInput(ARRAY_TYPE** array_in, repeats** array_count, ARRAY_TYPE** array_out, int* len);
bool count(ARRAY_TYPE** array_in, repeats** array_count, int len, int* repeats_count);
bool processing(ARRAY_TYPE** array_in, ARRAY_TYPE** array_out, int len, int* new_len);
bool dataOutput(ARRAY_TYPE** array_in, ARRAY_TYPE** array_out, int len, int new_len, repeats** array_count, int repeats_count);
#pragma endregion

signed main()
{
    SetConsoleOutputCP(65001);
    setlocale(LC_ALL, "ru_RU.utf8");
    ARRAY_TYPE* array_in = NULL;
    repeats* array_count = NULL;
    ARRAY_TYPE* array_out = NULL;
    int len = 0;
    int new_len = 0;
    int repeats_count = 0;
    dataInput(&array_in, &array_count, &array_out, &len);
    processing(&array_in, &array_out, len, &new_len);
    count(&array_in, &array_count, len, &repeats_count);
    dataOutput(&array_in, &array_out, len, new_len, &array_count, repeats_count);
    return 0;
}

bool dataInput(ARRAY_TYPE** array_in, ARRAY_TYPE** array_count, ARRAY_TYPE** array_out, int* len)
{
    printf_s("Введите длину массива: ");
    char term;
    while (*len == 0)
    {
        if (scanf_s("%d%c", len, &term) != 2 || (term != '\n' && term != ' ' && term != '\t') || *len <= 0)
        {
            *len = 0;
            fseek(stdin, 0, SEEK_END);
            printf_s("Ваш ввод не может являться длиной массива\n");
            printf_s("Введите длину массива: ");
        }
        else
        {
            *array_in = (ARRAY_TYPE*)malloc(*len * sizeof(ARRAY_TYPE));
            if (*array_in == NULL)
            {
                *len = 0;
                printf_s("Невозможно выделить достаточное количество памяти для хранения указателей.\nПопробуйте другое число\n");
            }
            else
            {
                *array_out = (ARRAY_TYPE*)malloc(*len * sizeof(ARRAY_TYPE));
                if (*array_out == NULL)
                {
                    free((void*)(*array_in));
                    *len = 0;
                    printf_s("Невозможно выделить достаточное количество памяти для хранения указателей.\nПопробуйте другое число\n");
                } 
                else
                {
                    *array_count = (repeats*)malloc(*len * sizeof(repeats));
                    if (*array_count == NULL)
                    {
                        free((void*)(*array_in));
                        free((void*)(*array_out));
                        *len = 0;
                        printf_s("Невозможно выделить достаточное количество памяти для хранения указателей.\nПопробуйте другое число\n");
                    }
                }
            }
        }
    }
    
    for (ARRAY_TYPE* i = *array_in; i < *array_in + *len; i++)
    {
        char term;
        printf_s("Введите элемент массива %d: ", i - *array_in);
        while (scanf_s("%d%c", i, &term) != 2 || (term != '\n' && term != ' ' && term != '\t'))
        {
            fseek(stdin, 0, SEEK_END);
            printf_s("Ваш ввод не может являться элементом матрицы\n");
            printf_s("Введите элемент массива %d: ", i - *array_in);
        }
    }
    printf_s("Исходный массив:\n");
    for (ARRAY_TYPE* i = *array_in; i < *array_in + *len; i++)
    {
        printf_s("%d ", *i);
    }
    printf_s("\n");
    return true;
}

bool count(ARRAY_TYPE** array_in, repeats** array_count, int len, int* repeats_count)
{
    for (int i = 0; i < len; i++)
    {
        int j = 0;
        while (j < *repeats_count && *((*array_in) + i) != (*((*array_count) + j)).number)
        {
            j++;
        }
        if (j >= *repeats_count)
        {
            (*((*array_count) + *repeats_count)) = (repeats){ *((*array_in) + i), 1 };
            (*repeats_count)++;
        }
        else
        {
            (*((*array_count) + j)).count++;
        }
    }
    return true;
}

bool processing(ARRAY_TYPE** array_in, ARRAY_TYPE** array_out, int len, int* new_len)
{
    ARRAY_TYPE* j = *array_out;
    for (ARRAY_TYPE* i = *array_in; i < *array_in + len; i++)
    {
        bool find = false;
        for (ARRAY_TYPE* l = *array_in; l < *array_in + len; l++)
        {
            if (l != i && *l == *i)
            {
                find = true;
            }
        }
        if (!find)
        {
            *(j++) = *i;
        }
    }
    *new_len = j - *array_out;
    return true;
}

bool dataOutput(ARRAY_TYPE** array_in, ARRAY_TYPE** array_out, int len, int new_len, repeats** array_count, int repeats_count)
{
    for (repeats* i = *array_count; i < *array_count + repeats_count; i++)
    {
        printf_s("Число %d повторяется %d раз\n", (*i).number, (*i).count);
    }
    if (new_len == 0)
    {
        printf_s("Нет нового массива");
    }
    else if (new_len == len)
    {
        printf_s("Массив не изменился");
    }
    else
    {
        printf_s("Результат:\n");
        for (ARRAY_TYPE* i = *array_out; i < *array_out + new_len; i++)
        {
            printf_s("%d ", *i);
        }
    }
    return true;
}