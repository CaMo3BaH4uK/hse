/*
    * Microsoft Visual Studio 2022
    * MSVC C++ Complier (v143)
    * C++ Language Standart: ISO C++20 Standard
    *
    * Lab 9
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#define NOMINMAX

#include <iostream>
#include <string>
#include <Windows.h>
#include <vector>
#include <limits>

typedef struct {
    int SourceString;
    std::string String;
} SubString;

void AddSubstring(std::vector<SubString>& SubStrings, std::string String, std::size_t StartPos, std::size_t Len, int Source)
{
    SubString temp;
    temp.String.assign(String, StartPos, Len);
    temp.SourceString = Source;
    SubStrings.push_back(temp);
}

int CountOpChars(std::string String)
{
    int Count = -1;
    int FoundPos = -1;
    int LastFoundPos = -1;
    do {
        FoundPos = String.find_first_of("+-/*", FoundPos + 1);
        if (FoundPos - LastFoundPos != 1) {
            FoundPos = std::string::npos;
        }
        Count++;
        LastFoundPos = FoundPos;
    } while (FoundPos != std::string::npos);

    return Count;
}

int InputStrings(std::vector<std::string>& InputStrings)
{
    int Count = 0;
    std::cout << "������� ���������� �����:\n";
    std::cin >> Count;
    while (std::cin.fail() || Count < 1) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        std::cout << "��� ���� �� �������� ����������� ���������\n";
        std::cout << "������� ���������� �����:\n";
        std::cin >> Count;
    }
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cout << "������� ������:\n";

    for (int i = 0; i < Count; i++) {
        std::string string;
        std::getline(std::cin, string);
        InputStrings.push_back(string);
    }

    return Count;
}

int FindSubstrings(std::vector<std::string>& InputStrings, int InputStringsVectCount, std::vector<SubString>& SubStrings)
{
    int Count = 0;
    for (int i = 0; i < InputStringsVectCount; i++) {
        std::string String = InputStrings[i];
        int FoundPos = -1;
        int LastFoundPos = -1;

        do {
            FoundPos = String.find_first_of("1234567890", FoundPos + 1);
            int StrLen = FoundPos == -1 ? String.size() - LastFoundPos - 1 : FoundPos - LastFoundPos - 1;
            if (StrLen > 0) {
                AddSubstring(SubStrings, String, LastFoundPos + 1, StrLen, i);
                Count++;
            }
            LastFoundPos = FoundPos;
        } while (FoundPos != std::string::npos);
    }
    return Count;
}

int FindMinimalSubstring(std::vector<SubString>& SubStrings, int SubStringsVectCount)
{
    int Min = CountOpChars(SubStrings[0].String);
    int MinNumber = 0;
    for (int i = 1; i < SubStringsVectCount; i++) {
        int CurrentStringCount = CountOpChars(SubStrings[i].String);
        if (CurrentStringCount < Min) {
            Min = CurrentStringCount;
            MinNumber = i;
        }
    }
    return MinNumber;
}

int DelNonAlpha(std::string& String)
{
    int Count = 0;
    int FoundPos = 0;
    while ( (FoundPos = String.find_first_not_of("qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM", FoundPos)) != std::string::npos) {
        String.erase(String.begin() + FoundPos);
        Count++;
    }

    return Count;
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");

    std::vector<std::string> InputStringsVect;
    std::vector<SubString> SubStringsVect;

    int InputStringsVectCount = 0;
    unsigned long long a, b;
    do {
        std::cout << "������� ���������� �����:\n";
        std::cin >> InputStringsVectCount;
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        a = std::cin.fail();
        b = std::cin.gcount();
    } while (std::cin.fail() || InputStringsVectCount < 1 || std::cin.gcount() > 1);


    std::cout << "������� ������:\n";

    for (int i = 0; i < InputStringsVectCount; i++) {
        std::string string;
        std::getline(std::cin, string);
        InputStringsVect.push_back(string);
    }

    int SubStringsVectCount = FindSubstrings(InputStringsVect, InputStringsVectCount, SubStringsVect);
    if (!SubStringsVectCount) {
        std::cout << "��������� �� �������\n";
        return 0;
    }
    else {
        std::cout << "������� ��������� ���������:\n";
        for (SubString& SubStringElement : SubStringsVect) {
            std::cout << "\"" << SubStringElement.String << "\" � �������� ������ �" << SubStringElement.SourceString + 1 << "\n";
        }
    }
    SubString MinSubString = SubStringsVect[FindMinimalSubstring(SubStringsVect, SubStringsVectCount)];
    std::cout << "��������� \"" << MinSubString.String << "\" � �������� ������ �" << MinSubString.SourceString + 1 << " ����� ������ ����� ������ ��������\n";
    int CountDeleted = DelNonAlpha(InputStringsVect[MinSubString.SourceString]);
    if (InputStringsVect[MinSubString.SourceString].length() == 0) {
        std::cout << "�������� ������ ��������� ����������\n";
    } else if (CountDeleted) {
        std::cout << "�������� ������ ����� ���������: " << InputStringsVect[MinSubString.SourceString] << "\n";
    }
    else {
        std::cout << "�������� ������ �� ����������\n";
    }
    return 0;
}
