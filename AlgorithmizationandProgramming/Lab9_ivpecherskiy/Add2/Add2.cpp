/*
    * Microsoft Visual Studio 2022
    * MSVC C++ Complier (v143)
    * C++ Language Standart: ISO C++20 Standard
    *
    * Lab 9_Add
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#define NOMINMAX

#include <iostream>
#include <string>
#include <Windows.h>
#include <algorithm>
#include <vector>

int SortSubstring(std::string& String)
{
    int FoundPos = -1;
    int LastFoundPos = -1;

    std::string Source = String;

    std::vector<std::string> SubStrings;

    int Status = 0;

    int Count = 0;

    do {
        FoundPos = String.find_first_not_of("1234567890", FoundPos + 1);
        int StrLen = FoundPos == -1 ? String.size() - LastFoundPos - 1 : FoundPos - LastFoundPos - 1;
        if (StrLen > 0) {
            SubStrings.push_back(String.substr(LastFoundPos + 1, StrLen));
            //String.erase(LastFoundPos + 1, StrLen);
            //String.insert(LastFoundPos + 1, "0");
            //Count++;
            //Status = 1;
            //if (FoundPos != std::string::npos) {
            //    FoundPos -= StrLen - 1;
            //}  
        }
        LastFoundPos = FoundPos;
    } while (FoundPos != std::string::npos);

    std::string temp;
    for (int i = 0; i < Count - 1; i++) {
        for (int j = 0; j < Count - i - 1; j++) {
            if (SubStrings[j].size() > SubStrings[j + 1].size()) {
                temp = SubStrings[j];
                SubStrings[j] = SubStrings[j + 1];
                SubStrings[j + 1] = temp;
            }
        }
    }

    for (int i = 0; i < Count; i++) {
        int PosToInsert = String.find("0");
        String.erase(String.begin() + PosToInsert);
        String.insert(PosToInsert, SubStrings[i]);
    }

    if (Source.compare(String)) {
        Status = 2;
    }

    return Status;
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");

    std::cout << "������� ������:\n";

    std::string String;
    std::getline(std::cin, String);

    int Status = SortSubstring(String);

    if (Status == 2) {
        std::cout << "����� ������: " << String << "\n";
    }
    else if (Status == 1) {
        std::cout << "������ �� ����������\n";
    }
    else {
        std::cout << "������ �� �������� �� �������\n";
    }

    return 0;
}
