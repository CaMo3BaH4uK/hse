/*
    * Microsoft Visual Studio 2022
    * MSVC C++ Complier (v143)
    * C++ Language Standart: ISO C++20 Standard
    *
    * Lab 9_Add
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#define NOMINMAX

#include <iostream>
#include <string>
#include <Windows.h>
#include <algorithm>

int SortSubstring(std::string& String)
{
    int FoundPos = -1;
    int LastFoundPos = -1;

    std::string Source = String;

    int Status = 0;

    do {
        FoundPos = String.find_first_not_of("1234567890", FoundPos + 1);
        int StrLen = FoundPos == -1 ? String.size() - LastFoundPos - 1 : FoundPos - LastFoundPos - 1;
        if (StrLen > 0) {
            if (!Status) {
                Status = 1;
            }
            std::sort(String.begin() + LastFoundPos + 1, String.begin() + LastFoundPos + 1 + StrLen);
        }
        LastFoundPos = FoundPos;
    } while (FoundPos != std::string::npos);
    if (Source.compare(String)) {
        Status = 2;
    }
    return Status;
}

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");

    std::cout << "������� ������:\n";

    std::string String;
    std::getline(std::cin, String);

    int Status = SortSubstring(String);

    if (Status == 2) {
        std::cout << "����� ������: " << String << "\n";
    }
    else if (Status == 1) {
        std::cout << "������ �� ����������\n";
    }
    else {
        std::cout << "������ �� �������� �� �������\n";
    }

    return 0;
}
