/*
	* Microsoft Visual Studio 2022
	* MSVC C++ Complier (v143)
	* C++ Language Standart: ISO C++20 Standard
	*
	* Lab 11_2
	* Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
	* x86_64 platform
*/

#define NOMINMAX

#include <iostream>
#include <chrono>
#include <limits>
#include <windows.h>
#include <string>
#include <map>

typedef struct _NODE
{
	int info;
	_NODE *left, *right;
} NODE, *PNODE;

PNODE newn(int p)
{
	PNODE node = new NODE;
	node->info = p;
	node->left = node->right = NULL;
	return(node);
}

void setleft(PNODE node, int x)
{
	node->left = newn(x);
}

void setright(PNODE node, int x)
{
	node->right = newn(x);
}

PNODE inputTree()
{
	int n;
	PNODE tree = NULL, next, cur;
	std::cout << "Введите целые числа\nБуква является концом ввода" << std::endl;
	std::cin >> n;
	if (!std::cin.fail()) {
		tree = newn(n);
		std::cin >> n;
		while (!std::cin.fail()) {
			next = cur = tree;
			while (next != NULL) {
				cur = next;
				if (n < cur->info)
					next = cur->left;
				else next = cur->right;
			}
			if (n < cur->info) setleft(cur, n);
			else setright(cur, n);
			std::cin >> n;
		}
	}
	return tree;
}

void countTree(PNODE tree, std::map<int, int> &numbers)
{
	if (tree) {
		countTree(tree->left, numbers);
		numbers[tree->info]++;
		countTree(tree->right, numbers);
	}
}

void outputTree(PNODE tree, int h = 0)
{
	if (tree) {
		outputTree(tree->left, h + 1);
		for (int i = 0; i < h; i++) std::cout << " ";
		std::cout << tree->info << std::endl;
		outputTree(tree->right, h + 1);
	}
}

int getMaxCountNumber(PNODE tree)
{
	std::map<int, int> numbers;
	countTree(tree, numbers);
	int curMaxElem = (numbers.begin())->first;
	int curMaxValue = (numbers.begin())->second;
	for (const auto &[key, value] : numbers) {
		if (value > curMaxValue) {
			curMaxElem = key;
			curMaxValue = value;
		}
	}
	return curMaxElem;
}

int main()
{
	PNODE tree = inputTree();

	if (tree) {
		outputTree(tree);

		int curMaxElem = getMaxCountNumber(tree);
		std::cout
			<< "Наиболее часто встречающимся числом является "
			<< curMaxElem
			<< "."
			<< std::endl;
	} else {
		std::cout << "Дерево пустое." << std::endl;
	}

	return 0;
}
