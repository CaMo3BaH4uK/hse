/*
	* Microsoft Visual Studio 2022
	* MSVC C++ Complier (v143)
	* C++ Language Standart: ISO C++20 Standard
	*
	* Lab 11_1
	* Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
	* x86_64 platform
*/

#define NOMINMAX

#include <iostream>
#include <chrono>
#include <limits>
#include <windows.h>
#include <string>


uint64_t recS(int n)
{
	if (n == 1) {
		return 1;
	} else if (n > 1) {
		return 2 * recS(n - 1) + 1;
	} else {
		return -1;
	}
}

uint64_t iterS(int n)
{
	if (n == 1) {
		return 1;
	} else if (n > 1) {
		uint64_t res = 1;
		int curN = 1;
		while (curN != n) {
			res = 2 * res + 1;
			curN++;
		}
		return res;
	} else {
		return -1;
	}
}

int main()
{
	setlocale(LC_ALL, "ru_ru.utf8");
	SetConsoleCP(65001);
	SetConsoleOutputCP(65001);

	int n = 2;
	do {
		if (std::cin.fail() || n < 1) {
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			std::cout << "Ваш ввод не подходит" << std::endl;
		}
		std::cout << "Введите N (N > 0): ";
		std::cin >> n;
	} while (std::cin.fail() || n < 1);

	auto recSStart = std::chrono::high_resolution_clock::now();
	uint64_t recRes = recS(n);
	auto recSEnd = std::chrono::high_resolution_clock::now();
	auto iterSStart = std::chrono::high_resolution_clock::now();
	uint64_t iterRes = iterS(n);
	auto iterSEnd = std::chrono::high_resolution_clock::now();
	uint64_t recResTime = std::chrono::duration_cast<std::chrono::microseconds>(recSEnd - recSStart).count();
	uint64_t iterResTime = std::chrono::duration_cast<std::chrono::microseconds>(iterSEnd - iterSStart).count();

	std::cout
		<< "Рекурсивный: "
		<< recResTime
		<< " мкс. Результат: "
		<< recRes
		<< std::endl;

	std::cout
		<< "Итерационный: "
		<< iterResTime
		<< " мкс. Результат: "
		<< iterRes
		<< std::endl;

	return 0;
}