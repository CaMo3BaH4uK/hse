/*
	* Microsoft Visual Studio 2022
	* MSVC C++ Complier (v143)
	* C++ Language Standart: ISO C++20 Standard
	*
	* Lab 10 Add
	* Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
	* x86_64 platform
*/
#define NOMINMAX

#include <iostream>
#include <limits>
#include <fstream>
#include <string>
#include <windows.h>

enum STATUS
{
	OK,
	GROUP_NOT_FOUNDED,
	FULL_DELETE
};

typedef struct _STUDENT {
	struct {
		std::string lastName, firstName, middleName;
	} name;
	std::string group;
	struct {
		int marks[5];
		int mid;
	} marks;
	struct _STUDENT* link = nullptr;
} STUDENT, * PSTUDENT;

void readFile(std::string filename, PSTUDENT& stackTop)
{
	std::string data;
	int sum;
	std::fstream f;
	f.open(filename);
	if (!f) {
		std::cout << "Файл не найдён" << std::endl;
		exit(1);
	}
	else if (f.peek() == EOF) {
		std::cout << "Файл пуст" << std::endl;
		exit(1);
	}
	else {
		while (!f.eof())
		{
			sum = 0;
			PSTUDENT stackCur = new STUDENT;
			getline(f, stackCur->name.lastName);
			getline(f, stackCur->name.firstName);
			getline(f, stackCur->name.middleName);
			getline(f, stackCur->group);
			for (int i = 0; i < 5; i++) {
				f >> stackCur->marks.marks[i];
				sum += stackCur->marks.marks[i];
			}
			f.ignore();
			stackCur->marks.mid = sum / 5;
			stackCur->link = stackTop;
			stackTop = stackCur;
		}
	}
	f.close();
}

void writeFile(std::string filename, PSTUDENT& stackTop)
{
	std::fstream f;
	f.open(filename, std::fstream::out);
	PSTUDENT stackCur = stackTop;
	while (stackCur)
	{
		f << stackCur->name.lastName << std::endl;
		f << stackCur->name.firstName << std::endl;
		f << stackCur->name.middleName << std::endl;
		f << stackCur->name.lastName << std::endl;
		f << stackCur->group << std::endl;
		for (int i = 0; i < 5; i++) {
			f << stackCur->marks.marks[i] << " ";
		}
		f << std::endl;
		stackCur = stackCur->link;
	}
	f.close();
}

enum STATUS process(PSTUDENT& stackTop)
{
	std::cin.ignore();

	std::string group;
	enum STATUS Status = STATUS::GROUP_NOT_FOUNDED;

	while (!group.length())
	{
		std::cout << "Введите искомую группу: ";
		std::cin >> group;
	}

	PSTUDENT stackCur = stackTop;
	PSTUDENT stackPrev = nullptr;
	while (stackCur)
	{
		if (stackCur->group == group)
		{
			Status = STATUS::OK;
			if (stackPrev)
			{
				stackPrev->link = stackCur->link;
			}
			else
			{
				stackTop = stackCur->link;
			}
			delete stackCur;
			stackCur = stackPrev ? stackPrev->link : stackTop;
		}
		else {
			stackPrev = stackCur;
			stackCur = stackCur->link;
		}
	}

	return stackTop ? Status : STATUS::FULL_DELETE;
}

void freeMemory(PSTUDENT stackTop) {
	PSTUDENT stackCur = stackTop;
	PSTUDENT stackNext = nullptr;
	while (stackCur)
	{
		stackNext = stackCur->link;
		delete stackCur;
		stackCur = stackNext;
	}
}

int main()
{
	setlocale(LC_ALL, "ru_ru.utf8");
	SetConsoleCP(65001);
	SetConsoleOutputCP(65001);
	PSTUDENT stackTop = nullptr;
	std::string filenameIn, filenameOut;
	std::cout << "Введите имя входного файла: ";
	std::cin >> filenameIn;
	std::cout << "Введите имя выходного файла: ";
	std::cin >> filenameOut;
	readFile(filenameIn, stackTop);
	enum STATUS Status = process(stackTop);
	switch (Status)
	{
	case OK:
		std::cout << "Стек отредактирован" << std::endl;
		break;
	case GROUP_NOT_FOUNDED:
		std::cout << "Группа не найдена. Стек не изменён" << std::endl;
		break;
	case FULL_DELETE:
		std::cout << "Полное очищение стека" << std::endl;
		break;
	default:
		break;
	}
	writeFile(filenameOut, stackTop);
	freeMemory(stackTop);
	return 0;
}