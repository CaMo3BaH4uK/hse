/*
    * Microsoft Visual Studio 2022
    * MSVC C Complier (v143)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 6_1
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#include "module.h"

INT main()
{
    FILE* InStream = stdin;
    FILE* OutStream = stdout;
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");

    BOOK Books[MAX_ARRAY_LEN];

    INT Count = BooksInput(InStream, OutStream, &Books);
    
    BooksOutput(OutStream, Books, Count, TRUE);

    CHAR PublishingHouses[MAX_ARRAY_LEN][MAX_STRING_LEN];
    INT FoundedPublichingHouses = MinumumPriceSearch(Books, Count, PublishingHouses);
    if (FoundedPublichingHouses) {
        PublishingHousesOutput(OutStream, PublishingHouses, FoundedPublichingHouses);
    }
    else {
        fprintf(OutStream, "������������ �� �������\n");
    }
    
    return 0;
}