#pragma once
#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <Windows.h>
#include <locale.h>
#include <time.h>
#include <limits.h>
#pragma endregion

#pragma region Defines
#define MAX_STRING_LEN  255
#define MAX_ARRAY_LEN   60

#define CLR             while (fgetc(InStream) != '\n');

#pragma warning(disable : 4996)
#pragma endregion

#pragma region Structs
typedef struct _PUBLISHING_HOUSE {
    INT                 year;
    CHAR                name[MAX_STRING_LEN];
    CHAR                city[MAX_STRING_LEN];
} PUBLISHING_HOUSE, * PPUBLISHING_HOUSE;

typedef struct _BOOK {
    CHAR                name[MAX_STRING_LEN];
    FLOAT               price;
    INT                 pages;
    CHAR                author[MAX_STRING_LEN];
    PUBLISHING_HOUSE    publishing;
} BOOK, * PBOOK;
#pragma endregion

VOID DeleteNewLine(
    IN  OUT CHAR* Str
)
{
    for (; *Str && *Str != '\n'; Str++);
    *Str = '\0';
}

BOOL NumberInput(
    IN      FILE* InStream,
    IN      FILE* OutStream,
    IN      INT     Min,
    IN      INT     Max,
    IN      CHAR* MessageString,
    OUT INT* Output
)
{
    if (InStream == stdin) {
        do {
            fprintf(OutStream, "������� %s:\n", MessageString);
            if (fscanf(InStream, "%d", Output) == 0) {
                CLR;
            }
        } while (*Output < Min || *Output > Max);
    }
    else {
        if (fscanf(InStream, "%d", Output) == 0 || *Output < Min || *Output > Max) {
            fprintf(OutStream, "������ ����� ������ '%s' \n", MessageString);
            CLR;
            return FALSE;
        }
    }
    CLR;
    return TRUE;
}

BOOL FloatInput(
    IN      FILE* InStream,
    IN      FILE* OutStream,
    IN      FLOAT   Min,
    IN      FLOAT   Max,
    IN      CHAR* MessageString,
    OUT FLOAT* Output
)
{
    if (InStream == stdin) {
        do {
            fprintf(OutStream, "������� %s:\n", MessageString);
            if (fscanf(InStream, "%f", Output) == 0) {
                CLR;
            }
        } while (*Output < Min || *Output > Max);
    }
    else {
        if (fscanf(InStream, "%f", Output) == 0 || *Output < Min || *Output > Max) {
            fprintf(OutStream, "������ ����� ������ '%s' \n", MessageString);
            CLR;
            return FALSE;
        }
    }
    CLR;
    return TRUE;
}

BOOL StringInput(
    IN      FILE* InStream,
    IN      FILE* OutStream,
    IN      CHAR* MessageString,
    OUT CHAR* Output
)
{
    if (InStream == stdin) {
        do {
            fprintf(OutStream, "������� %s:\n", MessageString);
            gets(Output);
        } while (strcmp(Output, "") == 0);
    }
    else {
        fgets(Output, 255, InStream);
        DeleteNewLine(Output);
        if (strcmp(Output, "") == 0) {
            fprintf(OutStream, "������ ����� ������ '%s' \n", MessageString);
        }
    }
    return TRUE;
}

INT BooksInput(
    IN      FILE* InStream,
    IN      FILE* OutStream,
    OUT PBOOK   Books
)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    INT CurrentYear = tm.tm_year + 1900;
    INT Count = 0;
    if (InStream == stdin) {
        NumberInput(InStream, OutStream, 1, MAX_ARRAY_LEN, "���������� ����", &Count);
        for (INT i = 0; i < Count; i++) {
            if (InStream == stdin) {
                fprintf(OutStream, "����� %d:\n", i + 1);
            }
            StringInput(InStream, OutStream, "��������", Books[i].name);
            FloatInput(InStream, OutStream, 0, INT_MAX, "���������", &(Books[i].price));
            NumberInput(InStream, OutStream, 0, INT_MAX, "����� �������", &(Books[i].pages));
            StringInput(InStream, OutStream, "������", &(Books[i].author));
            NumberInput(InStream, OutStream, 1500, CurrentYear, "��� �������", &(Books[i].publishing.year));
            StringInput(InStream, OutStream, "�������� ������������", Books[i].publishing.name);
            StringInput(InStream, OutStream, "����� ������������", Books[i].publishing.city);
        }
    }
    else {
        while (!feof(InStream)) {
            INT ErrorCount = 0;
            ErrorCount += !StringInput(InStream, OutStream, "��������", Books[Count].name);
            ErrorCount += !FloatInput(InStream, OutStream, 0, INT_MAX, "���������", &(Books[Count].price));
            ErrorCount += !NumberInput(InStream, OutStream, 0, INT_MAX, "����� �������", &(Books[Count].pages));
            ErrorCount += !StringInput(InStream, OutStream, "������", &(Books[Count].author));
            ErrorCount += !NumberInput(InStream, OutStream, 1500, CurrentYear, "��� �������", &(Books[Count].publishing.year));
            ErrorCount += !StringInput(InStream, OutStream, "�������� ������������", Books[Count].publishing.name);
            ErrorCount += !StringInput(InStream, OutStream, "����� ������������", Books[Count].publishing.city);
            if (ErrorCount) {
                fprintf(OutStream, "������ ����� �����. ��������� ������� ������\n");
            }
            else {
                Count++;
            }
        }
    }


    return Count;
}

VOID BooksOutput(
    IN      FILE* OutStream,
    IN      PBOOK   Books,
    IN      INT     Count,
    IN      BOOL    WaitForPress
)
{
    fprintf(OutStream, "\n�������� �����:\n");
    for (INT i = 0; i < Count; i++) {
        fprintf(OutStream, "\n����� �%d\n", i + 1);
        fprintf(OutStream, "��������: %s\n", Books[i].name);
        fprintf(OutStream, "����: %.2f\n", Books[i].price);
        fprintf(OutStream, "����� �������: %d\n", Books[i].pages);
        fprintf(OutStream, "�����: %s\n", Books[i].author);
        fprintf(OutStream, "������������:\n");
        fprintf(OutStream, "\t��� �������: %d\n", Books[i].publishing.year);
        fprintf(OutStream, "\t��������: %s\n", Books[i].publishing.name);
        fprintf(OutStream, "\t�����: %s\n", Books[i].publishing.city);
        if (WaitForPress) {
            fprintf(OutStream, "������� ����� ������ ��� �������� � ��������� �����\n");
            _getch();
        }
    }
}

INT Search(
    IN      PBOOK   Books,
    IN      INT     Count,
    OUT CHAR    BooksNames[MAX_ARRAY_LEN][MAX_STRING_LEN]
)
{
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    INT CurrentYear = tm.tm_year + 1900;
    INT year;
    NumberInput(stdin, stdout, 1500, CurrentYear, "��� �������", &year);
    INT FoundedBooks = 0;
    for (INT i = 0; i < Count; i++) {
        if (Books[i].pages == Books[i].price && Books[i].publishing.year > year) {
            int j;
            for (j = 0; j < FoundedBooks && strcmp(Books[i].name, BooksNames[j]); j++);
            if (j == FoundedBooks) {
                strcpy(BooksNames[j], Books[i].name);
                FoundedBooks++;
            }
        }
    }

    return FoundedBooks;
}

VOID FoundedBooksOutput(
    IN      FILE*   OutStream,
    IN      CHAR    BooksNames[MAX_ARRAY_LEN][MAX_STRING_LEN],
    IN      INT     FoundedBooks
)
{
    if (OutStream == stdout) {
        fprintf(OutStream, "���������� �����:\n");
    }
    for (INT i = 0; i < FoundedBooks; i++) {
        fprintf(OutStream, "%s\n", BooksNames[i]);
    }
}