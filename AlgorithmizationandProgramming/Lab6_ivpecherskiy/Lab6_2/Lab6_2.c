/*
    * Microsoft Visual Studio 2022
    * MSVC C Complier (v143)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 6_2
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/
#include "module.h"

FILE* OpenFileRead()
{
    CHAR FileName[MAX_STRING_LEN];
    FILE* FileStream;
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
    while ((FileStream = fopen(FileName, "r")) == NULL) {
        printf("�� ������� ������� ���� ��� ������. ���������� ��� ���\n");
        printf("������� ��� ����� ��� ������: ");
        gets(FileName);
    }
    return FileStream;
}

FILE* OpenFileWrite()
{
    CHAR FileName[MAX_STRING_LEN];
    FILE* FileStream;
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
    while ((FileStream = fopen(FileName, "w")) == NULL) {
        printf("�� ������� �������/������� ���� ��� ������. ���������� ��� ���\n");
        printf("������� ��� ����� ��� ������: ");
        gets(FileName);
    }
    return FileStream;
}

INT main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(LC_ALL, "ru_RU");
    FILE* InStream = OpenFileRead();
    fseek(InStream, 0, SEEK_END);
    if (ftell(InStream) < 5) {
        printf("���� ����. ���������� ���������\n");
        return 0;
    }
    rewind(InStream);
    FILE* OutStream = OpenFileWrite();

    BOOK Books[MAX_ARRAY_LEN];

    INT Count = BooksInput(InStream, stderr, &Books);
    if (!Count) {
        fprintf(stderr, "��� ����. ���������� ���������\n");
        return 0;
    }
    BooksOutput(stdout, Books, Count, FALSE);

    CHAR PublishingHouses[MAX_ARRAY_LEN][MAX_STRING_LEN];
    INT FoundedPublichingHouses = MinumumPriceSearch(Books, Count, PublishingHouses);
    if (FoundedPublichingHouses) {
        PublishingHousesOutput(OutStream, PublishingHouses, FoundedPublichingHouses);
    }
    else {
        fprintf(OutStream, "������������ �� �������\n");
    }

    return 0;
}