/*
    * Microsoft Visual Studio 2019
    * MSVC C Complier (v142)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 5_2
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <Windows.h>
#include <locale.h>
#include <math.h>
#pragma endregion

#pragma region Defines
#define MAX_STRING_LEN 255

#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)
#pragma endregion

#pragma region Functions
BOOL FileProcessing();
VOID DeleteNewLine(IN OUT CHAR* Str);
#pragma endregion

INT main()
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  setlocale(LC_ALL, "ru_RU");

  if (!FileProcessing()) {
    printf("���� ����");
  }


  return 0;
}

BOOL FileProcessing()
{
  BOOL NonEmptyString = FALSE;
  CHAR* FileName[MAX_STRING_LEN];

  FILE* FileReadStream;
  printf("������� ��� ����� ��� ������: ");
  gets(FileName);
  while ((FileReadStream = fopen(FileName, "r")) == NULL) {
    printf("�� ������� ������� ���� ��� ������. ���������� ��� ���\n");
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
  }

  FILE* FileWriteStream;
  printf("������� ��� ����� ��� ������: ");
  gets(FileName);
  while ((FileWriteStream = fopen(FileName, "w")) == NULL) {
    printf("�� ������� �������/������� ���� ��� ������. ���������� ��� ���\n");
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
  }

  while (!feof(FileReadStream)) {
    CHAR TempString[MAX_STRING_LEN];
    fgets(TempString, MAX_STRING_LEN, FileReadStream);

    if (feof(FileReadStream)) {
      printf("����� �����\n");
      return NonEmptyString;
    }

    DeleteNewLine(TempString);

    printf("�������� ������: ");
    if (*TempString == '\0' || *TempString == '\n' || *TempString == '\r') {
      printf("������ ������\n");
    }
    else {
      puts(TempString);
    }

    CHAR* Chr = TempString;
    Chr--;
    CHAR* NotDeleted = TempString;
    while (*Chr) {
      Chr++;
      INT Number = (INT)(*Chr - '0');
      if (!(Number >= 0 && Number <= 9 && Number % 2 == 0)) {
        *(NotDeleted++) = *Chr;
      }
    }

    fputs(TempString, FileWriteStream);
    fputc('\n', FileWriteStream);

    printf("�������� ������: ");
    if (*TempString == '\0' || *TempString == '\n' || *TempString == '\r') {
      printf("������ ������\n");
    }
    else {
      NonEmptyString = TRUE;
      puts(TempString);
    }
    printf("========================\n");
  }

  fclose(FileReadStream);
  fclose(FileWriteStream);

  return NonEmptyString;
}

VOID DeleteNewLine(
  IN OUT CHAR* Str
)
{
  for (; *Str && *Str != '\n'; Str++);
  *Str = '\0';
}
