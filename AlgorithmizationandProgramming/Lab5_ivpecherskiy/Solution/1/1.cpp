/*
    * Microsoft Visual Studio 2019
    * MSVC C Complier (v142)
    * C Language Standart: ISO C17 (2018) Standard
    *
    * Lab 5_1
    * Author: PECHERSKIY IVAN VASILEVICH <ivpecherskiy@edu.hse.ru>
    * x86_32 platform
*/

#pragma region Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <Windows.h>
#include <locale.h>
#include <math.h>
#pragma endregion

#pragma region Defines
#define MAX_STRING_LEN 255

#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)
#pragma endregion

#pragma region Functions
VOID FillFile();
BOOL ReadFileAndCalculate(OUT FLOAT* Result);
#pragma endregion

INT main()
{
  SetConsoleCP(1251);
  SetConsoleOutputCP(1251);
  setlocale(LC_ALL, "ru_RU");

  FillFile();

  FLOAT Result;
  if (ReadFileAndCalculate(&Result)) {
    printf("������������ �� ���������� �������� �����: %f", Result);
  } else {
    printf("������������ �� ���������� �������� �� �������");
  }

  return 0;
}


VOID FillFile ()
{
  CHAR FileName[MAX_STRING_LEN];
  FILE* FileStream;
  printf("������� ��� ����� ��� ������: ");
  gets(FileName);
  while ((FileStream = fopen(FileName, "w")) == NULL) {
    printf("�� ������� �������/������� ���� ��� ������. ���������� ��� ���\n");
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
  }

  INT N = 0;
  while (!N) {
    CHAR term;
    printf("������� ���������� �����: ");
    if (scanf("%d%c", &N, &term) != 2 ||
      (term != '\n' && term != ' ' && term != '\t') || N < 1) {
      N = 0;
      fseek(stdin, 0, SEEK_END);
      printf("���������� ��� ���\n");
    }
  }

  printf("������� ������ �����:\n");
  for (INT I = 0; I < N; I++) {
    FLOAT Temp;
    INT Result = scanf("%f", &Temp);
    if (Result != 1) {
      fseek(stdin, 0, SEEK_END);
      printf("����� ������ ���� ��������������. ���������� ��� ���:\n");
      I--;
    } else {
      fprintf(FileStream, "%f\n", Temp);
    }
  }
  fseek(stdin, 0, SEEK_END);
  fclose(FileStream);
}

BOOL ReadFileAndCalculate (
  OUT FLOAT *Max
  )
{
  CHAR FileName[MAX_STRING_LEN];
  FILE* FileStream;
  printf("������� ��� ����� ��� ������: ");
  gets(FileName);
  while ((FileStream = fopen(FileName, "r")) == NULL) {
    printf("�� ������� ������� ���� ��� ������. ���������� ��� ���\n");
    printf("������� ��� ����� ��� ������: ");
    gets(FileName);
  }
  
  *Max = 0;
  BOOL OneRead = FALSE;
  while (!feof(FileStream)) {
    FLOAT Temp;
    if (fscanf(FileStream, "%f", &Temp) == 1) {
      OneRead = TRUE;
      if (fabs(Temp) > fabs(*Max)) {
        *Max = Temp;
      }
    } else {
      CHAR C;
      while ((C=fgetc(FileStream)) != '\n' && !feof(FileStream));
    }
  }
  fclose(FileStream);
  return OneRead;
}
