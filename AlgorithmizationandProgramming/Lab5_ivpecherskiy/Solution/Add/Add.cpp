#include<stdio.h>
#include<string.h>
#include<ctype.h>
#include<stdlib.h>
#define lmax 100
#define _CRT_SECURE_NO_WARNINGS
#pragma warning(disable : 4996)
void file_name(char* filename)
{
  printf("Enter the name of your file:\n");
  gets(filename);
}

int file_outing(char* filename, char str[][lmax])
{
  FILE* file;
  int i = 0, f = 0;
  if (file = fopen(filename, "r")) {
    printf("There are strings from the file: %s\n", filename);
    while (!feof(file)) {
      fgets(*(str + i), lmax, file);
      if (**(str + i) != '\0' && **(str + i) != '\n') {
        printf(*(str + i++));
        f = 1;
      }
      else i++;
    }
    if (!f) {
      printf("There are no strings in file");
      exit(1);
    }
  }
  else {
    printf("Incorrect name of the file\n");
    exit(1);
  }
  return i;
}


void even(char str[][lmax], int i)
{
  int n;
  for (int j = 0; j < i; j++) {
    int k = 0;
    while (k < strlen(*(str + j))) {
      if (isdigit(str[j][k]) && str[j][k] % 2 == 0) {
        for (n = k; n < strlen(str[j]) - 1; n++)
          str[j][n] = str[j][n + 1];
        str[j][strlen(str[j]) - 1] = '\0';
      }
      else
        k++;
    }
  }
}

void new_file(char str[][lmax], int i)
{
  FILE* file;
  char filename[lmax];
  printf("\nEnter the name of the result file:\n");
  scanf("%s", filename);
  printf("\nStrings after the changes\n");
  file = fopen(filename, "w+");
  for (int k = 0; k < i; k++)
    if (**(str + k) != '\n')
      fputs(*(str + k), file);
  fclose(file);
  fopen(filename, "r");
  for (int k = 0; k < i; k++)
    printf(fgets(*(str + k), lmax, file));
}



int main()
{
  int newi, f;
  char str[lmax][lmax], filename[lmax];
  file_name(filename);
  int i = file_outing(filename, str);
  even(str, i);
  new_file(str, i);
  return 0;
}