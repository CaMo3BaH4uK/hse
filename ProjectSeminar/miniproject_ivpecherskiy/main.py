# -*- coding: utf-8 -*-

from statistics import mean
from collections import UserString
import json
import http.client
from datetime import datetime
from jinja2 import Template
import os
import plotly.graph_objects as go
import plotly.figure_factory as ff
import shutil
import ssl
from math import ceil

BaseFolder = "/var/www/html/students/ivpecherskiy"

UserEmails = []
UsersStats = {}

with open('/home/prsem/ivpecherskiy/ivpecherskiy/users.json') as json_file:
    UserEmails = json.load(json_file)

for Email in UserEmails:
    UserPrefix = Email.split("@")[0]
    UsersStats[UserPrefix] = {"name":"","avatar":"","Zulip":{},"Jitsi":{},"Taiga":{"projects":{},"epics":{},"userstories":{},"tasks":{}},"GitLab":{}}


def WeekOfMonth(dt):
    first_day = dt.replace(day=1)

    dom = dt.day
    adjusted_dom = dom + first_day.weekday()

    return int(ceil(adjusted_dom/7.0))

def zulipGetFullData(UserPrefix):
    for Domain in ["miem.hse.ru", "edu.hse.ru"]:
        conn = http.client.HTTPConnection("94.79.54.21", 3000)
        payload = json.dumps({
        "studEmail": "{}@{}".format(UserPrefix, Domain),
        "beginDate": "2021-09-01",
        "endDate": "2022-09-01",
        "token": "dIlUIIpKrjCcrmmM"
        })
        headers = {
        'Content-Type': 'application/json'
        }
        conn.request("POST", "/api/zulip/getDataPerMonth", payload, headers)
        res = conn.getresponse()
        data = res.read()
        if "{\"error\":\"no_info\"}" not in data.decode('utf-8'):
            return json.loads(data)
    return False

def zulipFillStats(UserPrefix):
    zulipUserData = zulipGetFullData(UserPrefix)
    if zulipUserData == False:
        return 0
    UsersStats[UserPrefix]["name"] = zulipUserData["full_name"]
    UsersStats[UserPrefix]["avatar"] = zulipUserData["avatar_url"]
    zulipUserMessages = zulipUserData["messages"]
    for zulipUserMessage in zulipUserMessages:
        try:
            UsersStats[UserPrefix]["Zulip"][zulipUserMessage["name"]] += [zulipUserMessage["timestamp"]]
        except:
            UsersStats[UserPrefix]["Zulip"][zulipUserMessage["name"]] = [zulipUserMessage["timestamp"]]

def gitlabGetFullData(UserPrefix):
    for Domain in ["miem.hse.ru", "edu.hse.ru"]:
        conn = http.client.HTTPConnection("94.79.54.21", 3000)
        payload = json.dumps({
        "studEmail": "{}@{}".format(UserPrefix, Domain),
        "beginDate": "2021-09-01",
        "endDate": "2022-09-01",
        "hideMerge": True,
        "token": "dIlUIIpKrjCcrmmM"
        })
        headers = {
        'Content-Type': 'application/json'
        }
        conn.request("POST", "/api/git/getDataPerMonth", payload, headers)
        res = conn.getresponse()
        data = res.read()
        if "{\"error\":\"no_data\"}" not in data.decode('utf-8'):
            return json.loads(data)
    return False

def gitlabFillStats(UserPrefix):
    GitLabUserData = gitlabGetFullData(UserPrefix)
    if not GitLabUserData:
        return 0
    GitLabProjects = GitLabUserData["projects"]
    for GitLabProject in GitLabProjects:
        UsersStats[UserPrefix]["GitLab"][GitLabProject["name"]] = []
        GitLabProjectCommits = GitLabProject["commits"]
        for GitLabProjectCommit in GitLabProjectCommits:
            UsersStats[UserPrefix]["GitLab"][GitLabProject["name"]] += [{"title":GitLabProjectCommit["title"], "date":GitLabProjectCommit["committed_date"], "total":GitLabProjectCommit["stats"]["total"]}]

def taigaAuth():
    conn = http.client.HTTPSConnection("track.miem.hse.ru", context = ssl._create_unverified_context())
    payload = json.dumps({
    "password": "S%grnfqcSJ6U77&",
    "type": "normal",
    "username": "ivpecherskiy"
    })
    headers = {
    'Content-Type': 'application/json'
    }
    conn.request("POST", "/api/v1/auth", payload, headers)
    res = conn.getresponse()
    data = res.read()
    UserData = json.loads(data)
    return UserData

def taigaGetProjects(UserID, Token):
    conn = http.client.HTTPSConnection("track.miem.hse.ru")
    payload = ''
    headers = {
    'Authorization': 'Bearer {}'.format(Token)
    }
    conn.request("GET", "/api/v1/projects?member={}".format(UserID), payload, headers)
    res = conn.getresponse()
    data = res.read()
    return json.loads(data)

def taigaGetUserInfo(UserID, Token):
    conn = http.client.HTTPSConnection("track.miem.hse.ru")
    payload = ''
    headers = {
    'Authorization': 'Bearer {}'.format(Token)
    }
    conn.request("GET", "/api/v1/users/{}".format(UserID), payload, headers)
    res = conn.getresponse()
    data = res.read()
    return json.loads(data)

def taigaGetUserStory(UserID, Token):
    conn = http.client.HTTPSConnection("track.miem.hse.ru")
    payload = ''
    headers = {
    'Authorization': 'Bearer {}'.format(Token)
    }
    conn.request("GET", "/api/v1/userstories?owner={}".format(UserID), payload, headers)
    res = conn.getresponse()
    data = res.read()
    return json.loads(data)

def taigaGetTasks(UserID, Token):
    conn = http.client.HTTPSConnection("track.miem.hse.ru")
    payload = ''
    headers = {
    'Authorization': 'Bearer {}'.format(Token)
    }
    conn.request("GET", "/api/v1/tasks?owner={}".format(UserID), payload, headers)
    res = conn.getresponse()
    data = res.read()
    return json.loads(data)

def taigaGetHistory(Type, ID, Token):
    conn = http.client.HTTPSConnection("track.miem.hse.ru")
    payload = ''
    headers = {
    'Authorization': 'Bearer {}'.format(Token)
    }
    conn.request("GET", "/api/v1/history/{}/{}".format(Type, ID), payload, headers)
    res = conn.getresponse()
    data = res.read()
    return json.loads(data)

def taigaGetEntity(Type, ID, Token):
    conn = http.client.HTTPSConnection("track.miem.hse.ru")
    payload = ''
    headers = {
    'Authorization': 'Bearer {}'.format(Token)
    }
    try:
        conn.request("GET", "/api/v1/{}/{}".format(Type, ID), payload, headers)
    except:
        return False
    res = conn.getresponse()
    data = res.read()
    return json.loads(data)

def taigaFillTree(TaigaToken, TaigaMyUserId):
    TaigaMyProjects = taigaGetProjects(TaigaMyUserId, TaigaToken)
    TaigaBivProjectMembers = [980]
    for TaigaBivProjectMember in TaigaBivProjectMembers:
        try:
            TaigaUser = taigaGetUserInfo(TaigaBivProjectMember, TaigaToken)
            next(True for UserEmail in UserEmails if TaigaUser["username"] in UserEmail)
            TaigaUserProjects = taigaGetProjects(TaigaBivProjectMember, TaigaToken)
            for TaigaUserProject in TaigaUserProjects:
                if TaigaUserProject["id"] not in UsersStats[TaigaUser["username"]]["Taiga"]["projects"]:
                    UsersStats[TaigaUser["username"]]["Taiga"]["projects"][TaigaUserProject["id"]] = {"name":TaigaUserProject["name"], "epicsids":[], "creation":TaigaUserProject["created_date"]}
            TaigaUserUserStories = taigaGetUserStory(TaigaBivProjectMember, TaigaToken)
            for TaigaUserUserStory in TaigaUserUserStories:
                Epics = [Epic for Epic in TaigaUserUserStory["epics"]]
                for Epic in Epics:
                    if Epic["id"] not in UsersStats[TaigaUser["username"]]["Taiga"]["epics"]:
                        UsersStats[TaigaUser["username"]]["Taiga"]["epics"][Epic["id"]] = {"name":Epic["subject"], "userstoriesids":[], "history":[]}
                        UsersStats[TaigaUser["username"]]["Taiga"]["projects"][Epic["project"]["id"]]["epicsids"] += [Epic["id"]]
                    if TaigaUserUserStory["id"] not in UsersStats[TaigaUser["username"]]["Taiga"]["epics"][Epic["id"]]["userstoriesids"]:
                        UsersStats[TaigaUser["username"]]["Taiga"]["epics"][Epic["id"]]["userstoriesids"] += [TaigaUserUserStory["id"]]
                if TaigaUserUserStory["id"] not in UsersStats[TaigaUser["username"]]["Taiga"]["userstories"]:
                    UsersStats[TaigaUser["username"]]["Taiga"]["userstories"][TaigaUserUserStory["id"]] = {"name":TaigaUserUserStory["subject"], "tasksids":[], "history":[]}
            TaigaUserTasks = taigaGetTasks(TaigaBivProjectMember, TaigaToken)
            for TaigaUserTask in TaigaUserTasks:
                if TaigaUserTask["id"] not in UsersStats[TaigaUser["username"]]["Taiga"]["tasks"]:
                    UsersStats[TaigaUser["username"]]["Taiga"]["tasks"][TaigaUserTask["id"]] = {"name":TaigaUserTask["subject"], "history":[]}
                    UsersStats[TaigaUser["username"]]["Taiga"]["userstories"][TaigaUserTask["user_story"]]["tasksids"] += [TaigaUserTask["id"]]
        except:
            pass

def taigaFillStats():
    TaigaMyUserData = taigaAuth()
    TaigaToken = TaigaMyUserData["auth_token"]
    TaigaMyUserId = TaigaMyUserData["id"]
    taigaFillTree(TaigaToken, TaigaMyUserId)
    for User, Data in UsersStats.items():
        if len(Data["Taiga"]["projects"]):
            for EpicID, EpicData in Data["Taiga"]["epics"].items():
                if not len(EpicData["history"]):
                    history = taigaGetHistory("epic", EpicID, TaigaToken)[::-1]
                    if len(history):
                        EpicEntity = False
                        while EpicEntity == False:
                            EpicEntity = taigaGetEntity("epics", EpicID, TaigaToken)
                        EpicData["history"] += [{"time":EpicEntity["created_date"], "status":"New"}]
                        for historyItem in history:
                            try:
                                EpicData["history"][-1]["status"] = historyItem["values_diff"]["status"][0]
                                EpicData["history"] += [{"time":historyItem["created_at"], "status":historyItem["values_diff"]["status"][-1]}]
                            except:
                                pass
                    else:
                        EpicEntity = False
                        while EpicEntity == False:
                            EpicEntity = taigaGetEntity("epics", EpicID, TaigaToken)
                        EpicData["history"] += [{"time":EpicEntity["created_date"], "status":"New"}]

            for UserstoryID, UserstoryData in Data["Taiga"]["userstories"].items():
                if not len(UserstoryData["history"]):
                    history = taigaGetHistory("userstory", UserstoryID, TaigaToken)[::-1]
                    if len(history):
                        UserstoryEntity = False
                        while UserstoryEntity == False:
                            UserstoryEntity = taigaGetEntity("userstories", UserstoryID, TaigaToken)
                        UserstoryData["history"] += [{"time":UserstoryEntity["created_date"], "status":"New"}]
                        for historyItem in history:
                            try:
                                UserstoryData["history"][-1]["status"] = historyItem["values_diff"]["status"][0]
                                UserstoryData["history"] += [{"time":historyItem["created_at"], "status":historyItem["values_diff"]["status"][-1]}]
                            except:
                                pass
                    else:
                        UserstoryEntity = False
                        while UserstoryEntity == False:
                            UserstoryEntity = taigaGetEntity("userstories", UserstoryID, TaigaToken)
                        UserstoryData["history"] += [{"time":UserstoryEntity["created_date"], "status":"New"}]
            
            for TaskID, TaskData in Data["Taiga"]["tasks"].items():
                if not len(TaskData["history"]):
                    history = taigaGetHistory("task", TaskID, TaigaToken)[::-1]
                    if len(history):
                        TaskEntity = False
                        while TaskEntity == False:
                            TaskEntity = taigaGetEntity("tasks", TaskID, TaigaToken)
                        
                        TaskData["history"] += [{"time":TaskEntity["created_date"], "status":"New"}]
                        for historyItem in history:
                            try:
                                TaskData["history"][-1]["status"] = historyItem["values_diff"]["status"][0]
                                TaskData["history"] += [{"time":historyItem["created_at"], "status":historyItem["values_diff"]["status"][-1]}]
                            except:
                                pass
                    else:
                        TaskEntity = False
                        while TaskEntity == False:
                            TaskEntity = taigaGetEntity("tasks", TaskID, TaigaToken)
                        TaskData["history"] += [{"time":TaskEntity["created_date"], "status":"New"}]

def jitsiGetSessions(UserPrefix):
    Result = []
    for Domain in ["miem.hse.ru", "edu.hse.ru"]:
        conn = http.client.HTTPConnection("94.79.54.21", 3000)
        payload = json.dumps({
        "studEmail": "{}@{}".format(UserPrefix, Domain),
        "beginDate": "2021-09-01",
        "endDate": "2022-09-01",
        "token": "dIlUIIpKrjCcrmmM"
        })
        headers = {
        'Content-Type': 'application/json'
        }
        conn.request("POST", "/api/jitsi/sessions", payload, headers)
        res = conn.getresponse()
        data = res.read()
        if "{\"error\":\"no_info\"}" not in data.decode('utf-8'):
            Result += json.loads(data) 
    return False if not len(Result) else Result

def jitsiFillStats(UserPrefix):
    jitsiUserSessions = jitsiGetSessions(UserPrefix)
    if jitsiUserSessions == False:
        return 0
    for jitsiUserSession in jitsiUserSessions:
        jitsiUserSessionBegin = datetime.fromisoformat("{}T{}".format(jitsiUserSession["date"],jitsiUserSession["begin"]))
        jitsiUserSessionEnd = datetime.fromisoformat("{}T{}".format(jitsiUserSession["date"],jitsiUserSession["end"]))
        jitsiUserSessionDuration = jitsiUserSessionEnd - jitsiUserSessionBegin
        try:
            UsersStats[UserPrefix]["Jitsi"][jitsiUserSession["room"]] += [{"begin": jitsiUserSessionBegin.isoformat(), "end": jitsiUserSessionEnd.isoformat(), "duration": jitsiUserSessionDuration.total_seconds()}]
        except:
            UsersStats[UserPrefix]["Jitsi"][jitsiUserSession["room"]] = [{"begin": jitsiUserSessionBegin.isoformat(), "end": jitsiUserSessionEnd.isoformat(), "duration": jitsiUserSessionDuration.total_seconds()}]

try:
    shutil.copytree('/home/prsem/ivpecherskiy/ivpecherskiy/server/assets', "{}/assets".format(BaseFolder)) 
except:
    pass

for UserPrefix in list(UsersStats.keys()):
    zulipFillStats(UserPrefix)
    gitlabFillStats(UserPrefix)
    jitsiFillStats(UserPrefix)
taigaFillStats()

for UserPrefix, UserData in UsersStats.items():
    if not len(UserData["name"]):
        UserData["name"] = UserPrefix

TemplateUserStats = []
for UserPrefix, UserData in UsersStats.items():
    try:    
        avatar = "https://chat.miem.hse.ru{}".format(UserData["avatar"]) if UserData["avatar"] is not None and len(UserData["avatar"]) else "https://memepedia.ru/wp-content/uploads/2018/07/cover1-768x512.jpg"
    except:
        avatar = "https://memepedia.ru/wp-content/uploads/2018/07/cover1-768x512.jpg"
    TemplateUserStat = {
        "name":UserData["name"],
        "prefix":UserPrefix,
        "avatar":avatar,
        "Zulip":{},
        "Jitsi":{},
        "Taiga":{"tasks":0},
        "GitLab":{}
        }
    try:
        TemplateUserStat["Zulip"]["channels"] = len(UserData["Zulip"])
    except:
        TemplateUserStat["Zulip"]["channels"] = 0
    try:
        TemplateUserStat["Zulip"]["all"] = sum([len(Messages) for Channel, Messages in UserData["Zulip"].items()])
    except:
        TemplateUserStat["Zulip"]["all"] = 0
    try:
        TemplateUserStat["Zulip"]["avg"] = round(mean([len(Messages) for Channel, Messages in UserData["Zulip"].items()]), 1)
    except:
        TemplateUserStat["Zulip"]["avg"] = 0
    try:
        TemplateUserStat["Zulip"]["fav"] = max(UserData["Zulip"], key=lambda key: len(UserData["Zulip"][key]))
    except:
        TemplateUserStat["Zulip"]["fav"] = 0

    TemplateUserStat["Zulip"]["ChannelsStats"] = {}
    TemplateUserStat["Zulip"]["ChannelsSummary"] = {}


    TemplateUserStat["Zulip"]["MessagesStats"] = {}
    for Channel, Messages in UserData["Zulip"].items():
        TemplateUserStat["Zulip"]["ChannelsSummary"][Channel] = len(Messages)
        for Message in Messages:
            MessageDate = datetime.fromisoformat(Message.replace("Z","")).strftime("%Y-%m-%d")
            try:
                TemplateUserStat["Zulip"]["MessagesStats"][MessageDate] += 1
            except:
                TemplateUserStat["Zulip"]["MessagesStats"][MessageDate] = 1
    TemplateUserStat["Zulip"]["MessagesStats"] = {k: TemplateUserStat["Zulip"]["MessagesStats"][k] for k in sorted(TemplateUserStat["Zulip"]["MessagesStats"])}


    fig = go.Figure(data=[go.Pie(labels=list(TemplateUserStat["Zulip"]["ChannelsSummary"].keys()),
                             values=list(TemplateUserStat["Zulip"]["ChannelsSummary"].values()))])
    fig.update_traces(textposition='inside', textinfo='value+label')
    TemplateUserStat["Zulip"]["MsgsPie"] = fig.to_html(full_html=False, include_plotlyjs='cdn')

    try:
        TemplateUserStat["Jitsi"]["rooms"] = sum([len(RoomData) for RoomData in list(UserData["Jitsi"].values())])
    except:
        TemplateUserStat["Jitsi"]["rooms"] = 0
    try:
        Durations = []
        for RoomData in list(UserData["Jitsi"].values()):
            for Session in RoomData:
                Durations += [Session["duration"]/60]
        TemplateUserStat["Jitsi"]["avg"] = round(mean(Durations), 1)
        TemplateUserStat["Jitsi"]["all"] = round(sum(Durations), 1)
    except:
        TemplateUserStat["Jitsi"]["avg"] = 0
        TemplateUserStat["Jitsi"]["all"] = 0
    try:
        TemplateUserStat["Jitsi"]["fav"] = max(UserData["Jitsi"], key=lambda key: len(UserData["Jitsi"][key]))
    except:
        TemplateUserStat["Jitsi"]["fav"] = 0

    TemplateUserStat["Jitsi"]["SessionsStats"] = {}
    TemplateUserStat["Jitsi"]["RoomsSummary"] = {}

    for Room, Sessions in UserData["Jitsi"].items():
        TemplateUserStat["Jitsi"]["RoomsSummary"][Room] = round(sum([Session["duration"]/60 for Session in Sessions]), 1)

        for Session in Sessions:
            SessionDateTime = datetime.fromisoformat(Session["begin"])
            SessionDate = "{}/{}".format(SessionDateTime.strftime("%Y-%m"), WeekOfMonth(SessionDateTime))
            try:
                TemplateUserStat["Jitsi"]["SessionsStats"][SessionDate] += 1
            except:
                TemplateUserStat["Jitsi"]["SessionsStats"][SessionDate] = 1
    TemplateUserStat["Jitsi"]["SessionsStats"] = {k: TemplateUserStat["Jitsi"]["SessionsStats"][k] for k in sorted(TemplateUserStat["Jitsi"]["SessionsStats"])}
    fig = go.Figure(data=[go.Pie(labels=list(TemplateUserStat["Jitsi"]["RoomsSummary"].keys()),
                             values=list(TemplateUserStat["Jitsi"]["RoomsSummary"].values()))])
    fig.update_traces(textposition='inside', textinfo='value+label')
    TemplateUserStat["Jitsi"]["RoomsPie"] = fig.to_html(full_html=False, include_plotlyjs='cdn')



    try:
        TemplateUserStat["GitLab"]["repos"] = len(UserData["GitLab"])
    except:
        TemplateUserStat["GitLab"]["repos"] = 0

    try:
        CommitsList = [len(Commits) for Repo, Commits in UserData["GitLab"].items()]
        TemplateUserStat["GitLab"]["commits"] = sum(CommitsList)
        TemplateUserStat["GitLab"]["avg"] = round(mean(CommitsList), 1)
    except:
        TemplateUserStat["GitLab"]["commits"] = 0
        TemplateUserStat["GitLab"]["avg"] = 0

    try:
        Changes = []
        for Repo, Commits in UserData["GitLab"].items():
            for Commit in Commits:
                Changes += [Commit["total"]]
        TemplateUserStat["GitLab"]["total"] = round(mean(Changes), 1)
    except:
        TemplateUserStat["GitLab"]["total"] = 0
    
    
    TemplateUserStat["GitLab"]["RepoStats"] = {}
    TemplateUserStat["GitLab"]["ActivitySummary"] = {}

    for Repo, Commits in UserData["GitLab"].items():
        TemplateUserStat["GitLab"]["RepoStats"][Repo] = {}
        for Commit in Commits:
            CommitDateTime = datetime.fromisoformat(Commit["date"].replace("Z",""))
            CommitDate = "{}/{}".format(CommitDateTime.strftime("%Y-%m"), WeekOfMonth(CommitDateTime))
            try:
                TemplateUserStat["GitLab"]["ActivitySummary"][CommitDate] += 1
            except:
                TemplateUserStat["GitLab"]["ActivitySummary"][CommitDate] = 1
            
            TemplateUserStat["GitLab"]["RepoStats"][Repo]["{}\n{}".format(Commit["title"], Commit["date"])] = Commit["total"]
    TemplateUserStat["GitLab"]["ActivitySummary"] = {k: TemplateUserStat["GitLab"]["ActivitySummary"][k] for k in sorted(TemplateUserStat["GitLab"]["ActivitySummary"])}

    CommitsCount = []
    for Commits in TemplateUserStat["GitLab"]["RepoStats"].values():
        CommitsCount += [len(Commits)]

    fig = go.Figure(data=[go.Pie(labels=list(TemplateUserStat["GitLab"]["RepoStats"].keys()),
                             values=CommitsCount)])
    fig.update_traces(textposition='inside', textinfo='value+label')
    TemplateUserStat["GitLab"]["ReposPie"] = fig.to_html(full_html=False, include_plotlyjs='cdn')

    try:
        TemplateUserStat["Taiga"]["projects"] = len(UserData["Taiga"]["projects"])
    except:
        TemplateUserStat["Taiga"]["projects"] = 0
    try:
        TemplateUserStat["Taiga"]["epics"] = len(UserData["Taiga"]["epics"])
    except:
        TemplateUserStat["Taiga"]["epics"] = 0
    try:
        TemplateUserStat["Taiga"]["userstories"] = len(UserData["Taiga"]["userstories"])
    except:
        TemplateUserStat["Taiga"]["userstories"] = 0
    try:
        TemplateUserStat["Taiga"]["tasks"] = len(UserData["Taiga"]["tasks"])
    except:
        TemplateUserStat["Taiga"]["tasks"] = 0

    TemplateUserStat["Taiga"]["TasksStats"] = {}
    NowIso = datetime.now().isoformat()
    TaigaDataFrame = []
    for TaigaProjectID, TaigaProjectData in UserData["Taiga"]["projects"].items():
        TaigaDataFrame += [dict(Task="{} {}  ".format(TaigaProjectID, TaigaProjectData["name"])[:40], Start=TaigaProjectData["creation"], Finish=NowIso, Resource="In progress")]
        for EpicID in TaigaProjectData["epicsids"]:
            Epic = UserData["Taiga"]["epics"][EpicID]
            for HistoryItemID in range(len(Epic["history"])-1):
                HistoryItem = Epic["history"][HistoryItemID]
                HistoryItemNext = Epic["history"][HistoryItemID+1]
                TaigaDataFrame += [dict(Task="{} {}  ".format(EpicID, Epic["name"])[:40], Start=HistoryItem["time"], Finish=HistoryItemNext["time"], Resource=HistoryItem["status"])]
            if Epic["history"][-1]["status"] == "Done":
                TaigaDataFrame += [dict(Task="{} {}  ".format(EpicID, Epic["name"])[:40], Start=Epic["history"][-1]["time"], Finish=Epic["history"][-1]["time"], Resource="Done")]
            else:
                TaigaDataFrame += [dict(Task="{} {}  ".format(EpicID, Epic["name"])[:40], Start=Epic["history"][-1]["time"], Finish=NowIso, Resource=Epic["history"][-1]["status"])]
            
            for UserstoryID in Epic["userstoriesids"]:
                UserStory = UserData["Taiga"]["userstories"][UserstoryID]
                for HistoryItemID in range(len(UserStory["history"])-1):
                    HistoryItem = UserStory["history"][HistoryItemID]
                    HistoryItemNext = UserStory["history"][HistoryItemID+1]
                    TaigaDataFrame += [dict(Task="{} {}  ".format(UserstoryID, UserStory["name"])[:40], Start=HistoryItem["time"], Finish=HistoryItemNext["time"], Resource=HistoryItem["status"])]
                if UserStory["history"][-1]["status"] == "Done":
                    TaigaDataFrame += [dict(Task="{} {}  ".format(UserstoryID, UserStory["name"])[:40], Start=UserStory["history"][-1]["time"], Finish=UserStory["history"][-1]["time"], Resource="Done")]
                else:
                    TaigaDataFrame += [dict(Task="{} {}  ".format(UserstoryID, UserStory["name"])[:40], Start=UserStory["history"][-1]["time"], Finish=NowIso, Resource=UserStory["history"][-1]["status"])]

                for TaskID in UserStory["tasksids"]:
                    Task = UserData["Taiga"]["tasks"][TaskID]
                    TaskDateTime = datetime.fromisoformat(Task["history"][0]["time"].replace("Z",""))
                    TaskDate = "{}/{}".format(TaskDateTime.strftime("%Y-%m"), WeekOfMonth(TaskDateTime))
                    try:
                        TemplateUserStat["Taiga"]["TasksStats"][TaskDate] += 1
                    except:
                        TemplateUserStat["Taiga"]["TasksStats"][TaskDate] = 1
                    for HistoryItemID in range(len(Task["history"])-1):
                        HistoryItem = Task["history"][HistoryItemID]
                        HistoryItemNext = Task["history"][HistoryItemID+1]
                        TaigaDataFrame += [dict(Task="{} {}  ".format(TaskID, Task["name"])[:40], Start=HistoryItem["time"], Finish=HistoryItemNext["time"], Resource=HistoryItem["status"])]
                    if Task["history"][-1]["status"] == "Closed":
                        TaigaDataFrame += [dict(Task="{} {}  ".format(TaskID, Task["name"])[:40], Start=Task["history"][-1]["time"], Finish=Task["history"][-1]["time"], Resource="Closed")]
                    else:
                        TaigaDataFrame += [dict(Task="{} {}  ".format(TaskID, Task["name"])[:40], Start=Task["history"][-1]["time"], Finish=NowIso, Resource=Task["history"][-1]["status"])]
    
    colors = {
        'New': 'rgb(252, 186, 3)',
        'In progress': 'rgb(173, 252, 3)',
        'Ready for test': 'rgb(3, 252, 136)',
        'Ready': 'rgb(3, 252, 136)',
        'Done': 'rgb(0, 255, 0)',
        'Closed': 'rgb(0, 255, 0)',
        'Archived': 'rgb(189, 148, 145)',
        'Needs Info': 'rgb(3, 198, 252)'
        }

    if len(TaigaDataFrame):
        fig = ff.create_gantt(TaigaDataFrame, colors=colors, index_col='Resource', show_colorbar=True,
                        group_tasks=True)
        TemplateUserStat["Taiga"]["GanttPlot"] = fig.to_html(full_html=False, include_plotlyjs='cdn')
    else:
        TemplateUserStat["Taiga"]["GanttPlot"] = ""

    TemplateUserStats += [TemplateUserStat]


for TemplateUserStat in TemplateUserStats:

    try:
        os.makedirs(BaseFolder)
    except:
        pass

    Days = list(TemplateUserStat["Zulip"]["MessagesStats"].keys())
    UserBars = list(TemplateUserStat["Zulip"]["MessagesStats"].values())
    fig = go.Figure(data=[
        go.Bar(name='User', x=Days, y=UserBars)
    ])
    fig.update_layout(barmode='group')
    TemplateUserStat["Zulip"]["MsgsPlot1"] = fig.to_html(full_html=False, include_plotlyjs='cdn')
    for i in range(1, len(UserBars)):
        UserBars[i] += UserBars[i-1]
    fig = go.Figure(data=go.Scatter(x=Days, y=UserBars))
    TemplateUserStat["Zulip"]["MsgsPlot2"] = fig.to_html(full_html=False, include_plotlyjs='cdn')

    Weeks = list(TemplateUserStat["Jitsi"]["SessionsStats"].keys())
    UserBars = list(TemplateUserStat["Jitsi"]["SessionsStats"].values())
    fig = go.Figure(data=[
        go.Bar(name='User', x=Weeks, y=UserBars)
    ])
    fig.update_layout(barmode='group')
    TemplateUserStat["Jitsi"]["RoomsPlot1"] = fig.to_html(full_html=False, include_plotlyjs='cdn')
    for i in range(1, len(UserBars)):
        UserBars[i] += UserBars[i-1]
    fig = go.Figure(data=go.Scatter(x=Weeks, y=UserBars))
    TemplateUserStat["Jitsi"]["RoomsPlot2"] = fig.to_html(full_html=False, include_plotlyjs='cdn')    

    Weeks = list(TemplateUserStat["GitLab"]["ActivitySummary"].keys())
    UserBars = list(TemplateUserStat["GitLab"]["ActivitySummary"].values())
    fig = go.Figure(data=[
        go.Bar(name='User', x=Weeks, y=UserBars)
    ])
    fig.update_layout(barmode='group')
    TemplateUserStat["GitLab"]["CommitsPlot1"] = fig.to_html(full_html=False, include_plotlyjs='cdn')
    for i in range(1, len(UserBars)):
        UserBars[i] += UserBars[i-1]
    fig = go.Figure(data=go.Scatter(x=Weeks, y=UserBars))
    TemplateUserStat["GitLab"]["CommitsPlot2"] = fig.to_html(full_html=False, include_plotlyjs='cdn')
    
    Weeks = list(TemplateUserStat["Taiga"]["TasksStats"].keys())
    UserBars = list(TemplateUserStat["Taiga"]["TasksStats"].values())
    for i in range(1, len(UserBars)):
        UserBars[i] += UserBars[i-1]
    fig = go.Figure(data=go.Scatter(x=Weeks, y=UserBars))
    TemplateUserStat["Taiga"]["Plot"] = fig.to_html(full_html=False, include_plotlyjs='cdn')
    
    TemplateUserStat["gen"] = datetime.now().isoformat()

    html = open("/home/prsem/ivpecherskiy/ivpecherskiy/server/User.j2", 'rb').read().decode('utf-8')
    template = Template(html)
    with open("{}/{}.html".format(BaseFolder, TemplateUserStat["prefix"]), 'wb') as fh:
        fh.write(template.render(User=TemplateUserStat).encode('utf-8'))

html = open('/home/prsem/ivpecherskiy/ivpecherskiy/server/Users.j2', 'rb').read().decode('utf-8')
template = Template(html)
with open("{}/Users.html".format(BaseFolder), 'wb') as fh:
    fh.write(template.render(Users=TemplateUserStats).encode('utf-8'))

html = open('/home/prsem/ivpecherskiy/ivpecherskiy/server/Landing.j2', 'rb').read().decode('utf-8')
template = Template(html)
ISODate = datetime.now().isoformat()
with open("{}/index.html".format(BaseFolder), 'wb') as fh:
    fh.write(template.render(ISODate=ISODate).encode('utf-8'))
