"""
Proxy service for CORS error escaping
"""

from flask import Blueprint, request, Response, stream_with_context
import requests


proxy = Blueprint('proxy', __name__)


methodRequestsMapping = {
    'GET': requests.get,
    'HEAD': requests.head,
    'POST': requests.post,
    'PUT': requests.put,
    'DELETE': requests.delete,
    'PATCH': requests.patch,
    'OPTIONS': requests.options
}


@proxy.route('/proxy/<path:url>', methods=methodRequestsMapping.keys())
def index(url):
    RequestsFunction = methodRequestsMapping[request.method]
    proxyRequest = RequestsFunction(url, stream=True, params=request.args)
    response = Response(stream_with_context(proxyRequest.iter_content()),
                        content_type=proxyRequest.headers['content-type'],
                        status=proxyRequest.status_code)
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response
