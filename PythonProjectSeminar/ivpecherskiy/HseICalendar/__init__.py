"""
The flask application package.
"""

from flask import Flask


def createApp(iconInstance, personInstance):
    app = Flask(__name__)

    app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

    from .views import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .settings import settings as settings_blueprint
    app.register_blueprint(settings_blueprint)

    from .proxy import proxy as proxy_blueprint
    app.register_blueprint(proxy_blueprint)

    from .actions import actions as actions_blueprint
    app.register_blueprint(actions_blueprint)

    app.config['ICON'] = iconInstance
    app.config['PERSON'] = personInstance

    app.add_template_global(personInstance, 'Person')

    return app
